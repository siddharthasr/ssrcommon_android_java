package ssr.android.ssrcommonlibrary.threads;

import android.util.Log;

import java.util.HashMap;

import sockettunnelling.entities.SendMessageToPeerRequestData;
import sockettunnelling.threads.SSRSocketP2PMessagePosterThread;

/**
 * Created on 2/16/2017.
 * Purpose : This class is android version of the class SSRSocketP2PMessagePosterThread
 */
public class SSRSocketP2PMessagePosterThreadForAndroid extends SSRSocketP2PMessagePosterThread
{
    public SSRSocketP2PMessagePosterThreadForAndroid(SendMessageToPeerRequestData i_messageRequestData)
            throws Exception
    {
        super(i_messageRequestData) ;
    }

    public SSRSocketP2PMessagePosterThreadForAndroid(HashMap<Object, Object> i_parametersHashMap,
                                                     SendMessageToPeerRequestData i_messageRequestData)
            throws Exception
    {
        super(i_parametersHashMap, i_messageRequestData) ;
    }

    public SSRSocketP2PMessagePosterThreadForAndroid(HashMap<Object, Object> i_parametersHashMap,
                                                     Integer i_socketTimeoutToSet,
                                                     SendMessageToPeerRequestData i_messageRequestData)
            throws Exception
    {
        super(i_parametersHashMap, i_socketTimeoutToSet, i_messageRequestData) ;
    }

    @Override
    protected void logMessage(String i_logMessage, Object i_auxiliaryParameter)
    {
        if(true == i_auxiliaryParameter instanceof Exception)
        {
            Log.e("ssrana", i_logMessage, (Exception)i_auxiliaryParameter) ;
        }
        else
        {
            Log.e("ssrana", i_logMessage);
        }
    }
}
