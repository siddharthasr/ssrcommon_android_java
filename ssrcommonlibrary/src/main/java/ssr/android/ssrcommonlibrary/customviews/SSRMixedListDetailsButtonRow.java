package ssr.android.ssrcommonlibrary.customviews;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import ssr.android.ssrcommonlibrary.R;

/**
 * Created on 1/25/2017.
 * Purpose : This class will be a SSRMixedList row which has a details button to add more details
 * Changelog :
 * 1) 20170205 :
 * Added possibility to set background colour of the row.
 */
public class SSRMixedListDetailsButtonRow extends SSRMixedListRow
{
    protected Integer m_rowViewId ;
    protected Integer m_detailsButtonId ;
    protected Integer m_labelTextViewId ;

    protected Integer m_rowBackgroundColourId ;

    protected ImageButton m_detailsButton ;
    protected TextView m_labelTextView ;

    protected String m_labelText ;
    protected View.OnClickListener m_detailsButtonOnClickListener ;
    protected View.OnClickListener m_labelTextOnClickListener ;
    protected Integer m_detailsButtonImageResourceId ;

    public SSRMixedListDetailsButtonRow() throws Exception
    {
        super() ;
        this.initializeMemberData() ;
        this.setViewIds() ;
    }

    public SSRMixedListDetailsButtonRow(String i_rowTextToStore) throws Exception
    {
        super(i_rowTextToStore);
        this.initializeMemberData() ;
        this.setLabelText(i_rowTextToStore) ;
        this.setViewIds() ;
    }

    @Override
    public View getViewToShow(LayoutInflater i_inflaterToUse, ViewGroup i_parentForView) throws Exception
    {
        View mainLayoutView = super.getViewToShow(i_inflaterToUse, i_parentForView) ;
        this.m_rowView = mainLayoutView.findViewById(this.m_rowViewId) ;
        this.storeViewReferences() ;
        this.setupViews() ;
        return this.m_rowView ;
    }

    public ImageButton getDetailsButton() throws Exception
    {
        return this.m_detailsButton ;
    }

    public TextView getLabelTextView() throws Exception
    {
        return this.m_labelTextView ;
    }

    public void setLabelText(String i_labelTextToSet) throws Exception
    {
        this.m_labelText = i_labelTextToSet ;

        if(null != this.m_labelTextView)
            this.m_labelTextView.setText(this.m_labelText) ;
    }

    public void setImageButtonImageId(Integer i_imageIdToUse) throws Exception
    {
        this.m_detailsButtonImageResourceId = i_imageIdToUse ;
    }

    public void setImageButtonOnClickListener(View.OnClickListener i_onClickListenerToSet) throws Exception
    {
        this.m_detailsButtonOnClickListener = i_onClickListenerToSet ;

        if(null != this.m_detailsButton)
            this.m_detailsButton.setOnClickListener(this.m_detailsButtonOnClickListener) ;
    }

    public void setLabelTextOnClickListener(View.OnClickListener i_onClickListenerToSet) throws Exception
    {
        this.m_labelTextOnClickListener = i_onClickListenerToSet ;

        if(null != this.m_labelTextOnClickListener)
            this.m_labelTextView.setOnClickListener(this.m_labelTextOnClickListener) ;
    }

    public void setRowBackgroundColourId(Integer i_rowBackgroundColourToSet) throws Exception
    {
        this.m_rowBackgroundColourId = i_rowBackgroundColourToSet ;
    }

    private void initializeMemberData() throws Exception
    {
        this.m_labelText = "" ;
    }

    private void setViewIds() throws Exception
    {
        this.m_rowViewId = R.id.mixedrows_textwithdetailsbutton_layout ;
        this.m_detailsButtonId = R.id.mixedrows_textwithdetailsbutton_detailsbutton ;
        this.m_labelTextViewId = R.id.mixedrows_textwithdetailsbutton_labeltextview ;
    }

    private void storeViewReferences() throws Exception
    {
        this.m_detailsButton = (ImageButton)this.m_rowView.findViewById(this.m_detailsButtonId) ;
        this.m_labelTextView = (TextView)this.m_rowView.findViewById(this.m_labelTextViewId) ;
    }

    private void setupViews() throws Exception
    {
        this.m_labelTextView.setText(this.m_labelText) ;
        if(null != this.m_detailsButtonImageResourceId)
            this.m_detailsButton.setImageResource(this.m_detailsButtonImageResourceId) ;

        if(null != this.m_detailsButtonOnClickListener)
            this.m_detailsButton.setOnClickListener(this.m_detailsButtonOnClickListener) ;

        if(null != this.m_labelTextOnClickListener)
            this.m_labelTextView.setOnClickListener(this.m_labelTextOnClickListener) ;

        if(null != this.m_rowBackgroundColourId)
            this.m_rowView.setBackgroundColor(this.m_rowBackgroundColourId) ;
    }
}
