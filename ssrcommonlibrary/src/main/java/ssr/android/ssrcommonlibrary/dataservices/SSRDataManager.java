package ssr.android.ssrcommonlibrary.dataservices;

import java.util.HashMap;

/**
 * Created by Siddhartha on 7/12/2017.
 * Purpose : This class will provide data management services. In the beginning this is only going
 *           to provide data sharing, and persistence (till application lifecycle) related service
 *           but in future it can be extended to give more services.
 */
public class SSRDataManager
{
    private static SSRDataManager ms_uniqueInstance ;

    protected Integer m_unsynchronizedDataHashMapLockObject ;

    protected HashMap<String, Object> m_unSynchronizedDataHashMap ;

    // -------- Singleton related methods ------------ //

    public static SSRDataManager getInstance() throws Exception
    {
        SSRDataManager toReturn = null ;
        if(null == SSRDataManager.ms_uniqueInstance)
        {
            SSRDataManager.createUniqueInstance() ;
            toReturn = SSRDataManager.ms_uniqueInstance ;
        }
        else
        {
            toReturn = SSRDataManager.ms_uniqueInstance ;
        }
        return toReturn ;
    }

    private static synchronized void createUniqueInstance() throws Exception
    {
        if(null == SSRDataManager.ms_uniqueInstance)
        {
            SSRDataManager.ms_uniqueInstance = new SSRDataManager() ;
        }
    }

    // --------- Constructors and destructors ------- //

    private SSRDataManager() throws Exception
    {
        this.m_unsynchronizedDataHashMapLockObject = new Integer(10) ;
        this.m_unSynchronizedDataHashMap = new HashMap<String, Object>() ;
    }


    // --------- Storage related public calls ------- //

    public void storeUnSynchronousData(String i_dataKey, Object i_data) throws Exception
    {
        synchronized(this.m_unsynchronizedDataHashMapLockObject)
        {
            this.m_unSynchronizedDataHashMap.put(i_dataKey, i_data) ;
        }
    }

    public Object getUnSynchronousData(String i_dataKey) throws Exception
    {
        Object toReturn = null ;
        synchronized(this.m_unsynchronizedDataHashMapLockObject)
        {
            toReturn = this.m_unSynchronizedDataHashMap.get(i_dataKey) ;
        }
        return toReturn ;
    }
}
