package ssr.android.ssrcommonlibrary.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import ssr.android.ssrcommonlibrary.R;
import ssr.android.ssrcommonlibrary.SSRFunctionalityLibrary;
import ssr.android.ssrcommonlibrary.adapters.SSRSelectableArrayAdapter;
import ssr.android.ssrcommonlibrary.entities.ContactData;

/**
 * Created on 6/8/2016.
 *
 * Purpose : This class is activity class to choose emergency contacts from a menu gui
 * Changelog :
 * 1) 20170725 :
 * Moved the original class from AegisRepository to SSRCommonLibrary for android. The activity
 * has now been modified so that it can be used as a supporting activity rather than having it
 * coupled with the original logic.
 */
public class SelectContactsActivity extends Activity implements View.OnClickListener
{

    public static final String SELECTED_CONTACTS_VECTOR = "SELECTED_CONTACTS_VECTOR" ;
    public static final String SELECTED_CONTACTS_RESULT_HASHMAP = "SELECTED_CONTACTS_RESULT_HASHMAP" ;

    // Under the hood variables
    protected Integer m_unSelectedContactsListViewId ;
    protected Integer m_selectedContactsListViewId ;
    protected Integer m_moveSelectedToUnSelectedButtonId ;
    protected Integer m_moveUnSelectedToSelectedButtonId ;
    protected Integer m_finalizeContactsSelectionButtonId ;
    protected Integer m_cancelContactsSelectionButtonId ;

    protected ListView m_unSelectedContactsListView ;
    protected ListView m_selectedContactsListView ;
    protected Button m_moveSelectedToUnSelectedButton ;
    protected Button m_moveUnSelectedToSelectedButton ;
    protected Button m_finalizeContactsSelectionButton ;
    protected Button m_cancelContactsSelectionButton ;

    protected HashMap<String, ContactData> m_selectedContactsHashMap ;
    protected HashMap<String, ContactData> m_unSelectedContactsHashMap;

    protected void init()
    {
        this.m_selectedContactsHashMap = new HashMap<String, ContactData>() ;
        this.m_unSelectedContactsHashMap = new HashMap<String, ContactData>() ;
    }

    protected void loadAndPrepareContactsDetails()
    {
        Vector<String> selectedContactsVector = (Vector<String>)this.getIntent().getExtras()
                .getSerializable(SelectContactsActivity.SELECTED_CONTACTS_VECTOR) ;

        HashMap<String, ContactData> contactsFromDeviceContacts = new HashMap<String, ContactData>() ;
        SSRFunctionalityLibrary.getPhoneContactsAsContactDataHashMap(this.getContentResolver(),
                contactsFromDeviceContacts) ;
        Iterator<String> contactsIterator = contactsFromDeviceContacts.keySet().iterator() ;
        while(contactsIterator.hasNext())
        {
            String oneContactKey = contactsIterator.next() ;
            if(false == selectedContactsVector.contains(oneContactKey))
            {
                this.m_unSelectedContactsHashMap.put(oneContactKey, contactsFromDeviceContacts
                        .get(oneContactKey)) ;
            }
            else
            {
                this.m_selectedContactsHashMap.put(oneContactKey, contactsFromDeviceContacts
                        .get(oneContactKey)) ;
            }
        }
    }

    /* This method is going to populate the data for the emergency contacts selection liss,
       and then display them, i.e. update the adapter etc..
     */
    protected void updateLists()
    {
        // TODO : Figure out if there can be any way to do this more efficiently,
        //        e.g. if the adapter / underlying array can be saved and then
        //        delta change is done rather than recalculating the entire thing.

        SSRSelectableArrayAdapter<Object> dataForUnSelectedContactsListView =
                new SSRSelectableArrayAdapter<Object> (this, R.layout.misc_components_layout, R.id.simple_largetext_textview,
                this.m_unSelectedContactsHashMap.keySet().toArray()) ;
        this.m_unSelectedContactsListView.setAdapter(dataForUnSelectedContactsListView) ;
        this.m_unSelectedContactsListView.setOnItemClickListener(dataForUnSelectedContactsListView);

        SSRSelectableArrayAdapter<Object> dataForSelectedContactsListView =
                new SSRSelectableArrayAdapter<Object> (this, R.layout.misc_components_layout, R.id.simple_largetext_textview,
                this.m_selectedContactsHashMap.keySet().toArray()) ;
        this.m_selectedContactsListView.setAdapter(dataForSelectedContactsListView) ;
        this.m_selectedContactsListView.setOnItemClickListener(dataForSelectedContactsListView);
    }

    protected void moveChosenUnSelectedContactsToSelected()
    {
        Vector<Object> chosenUnSelectedContactsToMove =
                ((SSRSelectableArrayAdapter<Object>)this.m_unSelectedContactsListView.getAdapter()).getSelectedItems() ;
        Iterator itemsToMoveIterator = chosenUnSelectedContactsToMove.iterator() ;
        while(itemsToMoveIterator.hasNext())
        {
            String oneItemToMove = new String(itemsToMoveIterator.next().toString()) ;
            ContactData oneContactData =
                    new ContactData(this.m_unSelectedContactsHashMap.get(oneItemToMove)) ;
            this.m_selectedContactsHashMap.put(oneItemToMove, oneContactData) ;
            this.m_unSelectedContactsHashMap.remove(oneItemToMove) ;
        }
        this.updateLists() ;
    }

    protected void moveChosenSelectedContactsToUnSelected()
    {
        Vector<Object> chosenSelectedContactsToMove =
                ((SSRSelectableArrayAdapter<Object>)this.m_selectedContactsListView.getAdapter()).getSelectedItems() ;
        Iterator itemsToMoveIterator = chosenSelectedContactsToMove.iterator() ;
        while(itemsToMoveIterator.hasNext())
        {
            String oneItemToMove = new String(itemsToMoveIterator.next().toString()) ;
            ContactData oneContactData =
                    new ContactData(this.m_selectedContactsHashMap.get(oneItemToMove)) ;
            this.m_unSelectedContactsHashMap.put(oneItemToMove, oneContactData) ;
            this.m_selectedContactsHashMap.remove(oneItemToMove) ;
        }
        this.updateLists() ;
    }

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_contacts);

        this.storeViewReferences() ;
        this.init() ;

        // Create universal handlers
        try
        {
            SSRFunctionalityLibrary.createUniversalHandlers();
        }
        catch(Exception e)
        {
            Log.e("ssrana", "Exception caught in SelectContactsActivity.onCreate :\n", e) ;
        }

        // set button click listeners
        this.setButtonsClickListener() ;

        this.loadAndPrepareContactsDetails() ;
        this.updateLists() ;
    }

    private void storeViewReferences()
    {
        this.m_unSelectedContactsListViewId = R.id.unSelectedContactsListView ;
        this.m_selectedContactsListViewId = R.id.selectedContactsListView ;
        this.m_moveSelectedToUnSelectedButtonId = R.id.moveSelectedToUnSelectedButton ;
        this.m_moveUnSelectedToSelectedButtonId = R.id.moveUnSelectedToSelectedButton  ;
        this.m_finalizeContactsSelectionButtonId = R.id.finalizeContactsSelection ;
        this.m_cancelContactsSelectionButtonId = R.id.cancelContactsSelection ;

        this.m_unSelectedContactsListView = (ListView)this.findViewById(this.m_unSelectedContactsListViewId) ;
        this.m_selectedContactsListView = (ListView)this.findViewById(this.m_selectedContactsListViewId) ;
        this.m_moveSelectedToUnSelectedButton = (Button)this.findViewById(this.m_moveSelectedToUnSelectedButtonId) ;
        this.m_moveUnSelectedToSelectedButton = (Button)this.findViewById(this.m_moveUnSelectedToSelectedButtonId) ;
        this.m_finalizeContactsSelectionButton = (Button)this.findViewById(this.m_finalizeContactsSelectionButtonId) ;
        this.m_cancelContactsSelectionButton = (Button)this.findViewById(this.m_cancelContactsSelectionButtonId) ;
    }

    protected void setButtonsClickListener()
    {
        Button cancelButton = (Button)findViewById(R.id.cancelContactsSelection) ;
        cancelButton.setOnClickListener(this) ;
        Button moveSelectedNonEmergencyToEmergencyButton = (Button)findViewById(R.id.moveUnSelectedToSelectedButton) ;
        moveSelectedNonEmergencyToEmergencyButton.setOnClickListener(this) ;
        Button moveSelectedEmergencyToNonEmergencyButton = (Button)findViewById(R.id.moveSelectedToUnSelectedButton) ;
        moveSelectedEmergencyToNonEmergencyButton.setOnClickListener(this) ;
        Button finalizeEmergencyContactsButton = (Button)findViewById(R.id.finalizeContactsSelection) ;
        finalizeEmergencyContactsButton.setOnClickListener(this) ;
    }

    protected void finalizeEmergencyContacts()
    {
        Intent intentForSendingResult = new Intent() ;
        intentForSendingResult.putExtra(SelectContactsActivity.SELECTED_CONTACTS_RESULT_HASHMAP,
                this.m_selectedContactsHashMap) ;
        this.setResult(Activity.RESULT_OK, intentForSendingResult) ;
        this.finish() ;
    }

    public void onClick(View v)
    {
        int id = v.getId();

        if(R.id.moveUnSelectedToSelectedButton == id)
        {
            this.moveChosenUnSelectedContactsToSelected();
        }
        else if(R.id.moveSelectedToUnSelectedButton == id)
        {
            this.moveChosenSelectedContactsToUnSelected();
        }
        else if(R.id.finalizeContactsSelection == id)
        {
            this.finalizeEmergencyContacts();
            Intent intent = NavUtils.getParentActivityIntent(this);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            this.startActivity(intent);
        }
        else if(R.id.cancelContactsSelection == id)
        {
            // Make no changes to emergency contacts list and go back to parent
            //NavUtils.navigateUpFromSameTask(this) ;
            this.finish();
            Intent intent = NavUtils.getParentActivityIntent(this);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            this.startActivity(intent);
        }
    }
}
