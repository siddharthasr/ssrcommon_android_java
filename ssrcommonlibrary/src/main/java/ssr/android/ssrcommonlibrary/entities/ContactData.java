package ssr.android.ssrcommonlibrary.entities;

import java.io.Serializable;

/**
 * Created on 6/9/2016.
 * Purpose : Data structure and related methods for containing data for emergency contacts
 * Changelog:
 * 20170725 -
 * 1) Class moved to SSRCommonLibrary project and renamed from EmergencyContactsData to
 *    ContactData.
 */
public class ContactData implements Serializable
{

    protected String m_name;
    protected String m_hasPhoneNumber;
    protected String m_uri;
    protected String m_imei ;
    protected String m_phoneNumber ;

    public ContactData()
    {
        this.m_name = new String("");
        this.m_hasPhoneNumber = new String("0");
        this.m_uri = new String("");
        this.m_imei = new String("") ;
        this.m_phoneNumber = new String("") ;
    }

    public ContactData(ContactData i_toCopy)
    {
        this.m_name = i_toCopy.getName() ;
        this.m_hasPhoneNumber = i_toCopy.getHasPhoneNumber() ;
        this.m_uri = i_toCopy.getUri() ;
        this.m_imei = i_toCopy.m_imei ;
        this.m_phoneNumber = i_toCopy.m_phoneNumber ;
    }

    public String getName()
    {
        return this.m_name;
    }

    public String getHasPhoneNumber()
    {
        return this.m_hasPhoneNumber ;
    }

    public String getUri()
    {
        return this.m_uri ;
    }

    public String getImei()
    {
        return this.m_imei ;
    }

    public String getPhoneNumber()
    {
        return this.m_phoneNumber ;
    }

    public void setName(String i_name)
    {
        this.m_name = i_name;
    }

    public void setHasPhoneNumber(String i_hasPhoneNumber)
    {
        this.m_hasPhoneNumber = i_hasPhoneNumber;
    }

    public void setUri(String i_uri)
    {
        this.m_uri = i_uri;
    }

    public void setImei(String i_imei)
    {
        this.m_imei = i_imei;
    }

    public void setPhoneNumber(String i_phoneNumber)
    {
        this.m_phoneNumber = i_phoneNumber;
    }
}
