package ssr.android.ssrcommonlibrary.adapters;

import android.os.Bundle;

import ssr.android.ssrcommonlibrary.fragments.PlaceholderFragment;

/**
 * Created on 8/6/2016.
 */
public class SSRGenericFragmentPagerAdapterTuple
{
    public Integer m_sectionNumber ; // Section number starts from 1
    public String m_title ;
    public Class<? extends PlaceholderFragment> m_fragmentClass ;

    // This is supposed to contain the layout resource as well, for key value PlaceholderFragment.ARG_LAYOUT_RESOURCE
    public Bundle m_fragmentArgsBundle ;
}
