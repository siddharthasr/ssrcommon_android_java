package ssr.android.ssrcommonlibrary.handlers;

/**
 * Created on 12/21/2016.
 */
public interface ISSRHandlerCallback
{
    // This will and should get called only from the UI thread.
    public void doPostHandlerProcessing(Object i_inputParameter) ;
}
