package ssr.android.ssrcommonlibrary.customviews;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.HashMap;

import ssr.android.ssrcommonlibrary.R;

/**
 * Created on 8/11/2016.
 */
public class SSRMixedListRow
{
    public String m_listRowText ;
    public Integer m_layoutResourceToInflate ;
    public View m_rowView ;
    public SSRMixedListRowHelperInterface m_onCreateHelper ;
    public String m_rowIdentifier ;

    public SSRMixedListRow() throws Exception
    {
        // Empty constructor implementation
    }

    public SSRMixedListRow(String i_rowTextToStore) throws Exception
    {
        this.m_listRowText = i_rowTextToStore ;
    }

    public String toString()
    {
        return (this.m_listRowText == null ? "null" : this.m_listRowText) ;
    }

    // It is to be noted, that the onClickHandler for the view has to be set when the view is
    // created and returned by the method below.
    public View getViewToShow(LayoutInflater i_inflaterToUse, ViewGroup i_parentForView)
            throws Exception
    {
        if(null != this.m_rowView)
            return this.m_rowView ;

        View viewToReturn = i_inflaterToUse.inflate((null == this.m_layoutResourceToInflate)
                ? R.layout.mixedlist_rows_layout : this.m_layoutResourceToInflate,
                i_parentForView, false) ;
        //viewToReturn.setOnClickListener(this) ;
        this.m_rowView = viewToReturn ;
        return viewToReturn ;
    }

    public void putResult(HashMap<String, Object> o_hashMapToPutResultsIn) throws Exception
    {
        // Empty, i.e. no result to be put.
    }

    /*public void handleClick(AdapterView<?> parent, View view, int position, long id)
    {
        return ;
    }*/
}
