package ssr.android.ssrcommonlibrary.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created on 8/6/2016.
 * Purpose : This class is to serve as the basic Fragment to be used in AegisApp. To use this fragment,
 *           it needs to be instantiated and then a setArgs needs to be called on it, with its layout
 *           in the input Bundle object with the key ARG_LAYOUT_RESOURCE.
 */
public class PlaceholderFragment extends Fragment
{
    // Section numbers start with 1, unlike position which starts from 0
    public static final String ARG_SECTION_NUMBER = "section_number";
    public static final String ARG_LAYOUT_RESOURCE = "layout_resource";

    // The view to be returned

    public void setArguments(Bundle i_argsToSet)
    {
        this.setArgumentsToRetain(i_argsToSet) ;
        super.setArguments(i_argsToSet) ;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(getArguments().getInt(ARG_LAYOUT_RESOURCE), container, false);
        return rootView;
    }

    protected void setArgumentsToRetain(Bundle i_argsToSet)
    {
        return ;
    }
}
