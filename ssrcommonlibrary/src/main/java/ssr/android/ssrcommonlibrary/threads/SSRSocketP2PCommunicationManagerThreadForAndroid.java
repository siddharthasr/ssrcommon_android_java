package ssr.android.ssrcommonlibrary.threads;

import android.util.Log;

import java.util.HashMap;

import sockettunnelling.threads.SSRSocketP2PCommunicationManagerThread;
import ssrcommon.threads.SSRBasicBackendServicesThread;

/**
 * Created on 2/7/2017.
 * Purpose : This class is android version of the class SSRSocketP2PCommunicationManagerThread
 */
public class SSRSocketP2PCommunicationManagerThreadForAndroid extends SSRSocketP2PCommunicationManagerThread
{
    public SSRSocketP2PCommunicationManagerThreadForAndroid(HashMap<Object, Object> i_parametersHashMap)
            throws Exception
    {
        this(i_parametersHashMap, SSRBasicBackendServicesThread.DEFAULT_SOCKET_CONNECTION_TIMEOUT_DURATION,
                SSRSocketP2PCommunicationManagerThread.DEFAULT_POLLING_GAP_DURATION);
    }

    public SSRSocketP2PCommunicationManagerThreadForAndroid(HashMap<Object, Object> i_parametersHashMap,
                                                            Integer i_socketTimeoutToSet, Integer i_pollingGapToSet)
            throws Exception
    {
        super(i_parametersHashMap, i_socketTimeoutToSet, i_pollingGapToSet) ;
    }

    @Override
    protected void logMessage(String i_logMessage, Object i_auxiliaryParameter)
    {
        if(true == i_auxiliaryParameter instanceof Exception)
        {
            Log.e("ssrana", i_logMessage, (Exception)i_auxiliaryParameter) ;
        }
        else
        {
            Log.e("ssrana", i_logMessage);
        }
    }
}
