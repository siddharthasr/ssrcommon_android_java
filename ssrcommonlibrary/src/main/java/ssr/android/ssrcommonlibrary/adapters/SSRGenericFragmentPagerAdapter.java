package ssr.android.ssrcommonlibrary.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import ssr.android.ssrcommonlibrary.fragments.PlaceholderFragment;

import java.util.HashMap;

/**
 * Created on 8/6/2016.
 * Purpose : This class is supposed to act as the basic, generic fragments pager adapter. To use this class, an
 *           object of this class needs to be instantiated with FragmentManager passed as input argument along
 *           with a HashMap which has position of fragments as the key, and details about the fragment, inside
 *           an object of SSRGEnericFragmentPagerAdapterTuple object, as the stored value.
 */
public class SSRGenericFragmentPagerAdapter extends FragmentStatePagerAdapter
{
    // Key to this hashmap is the position, which starts from 0 and not section which starts from 1
    HashMap<Integer, SSRGenericFragmentPagerAdapterTuple> m_fragmentsToHandleHashMap ;

    public SSRGenericFragmentPagerAdapter(FragmentManager i_fragmentManagerToUse,
                                          HashMap<Integer, SSRGenericFragmentPagerAdapterTuple> i_fragmentsToHandleHashMap)
    {
        super(i_fragmentManagerToUse) ;
        this.m_fragmentsToHandleHashMap = i_fragmentsToHandleHashMap ;
    }

    @Override
    public Fragment getItem(int position)
    {
        PlaceholderFragment fragmentToReturn = null ;
        SSRGenericFragmentPagerAdapterTuple oneFragmentTuple = this.m_fragmentsToHandleHashMap.get(position);
        try
        {
            fragmentToReturn = oneFragmentTuple.m_fragmentClass.newInstance() ;
        }
        catch(Exception e)
        {
            Log.e("ssrana", "Exception caught while trying to create fragment :\n", e) ;
            return null ;
        }
        Bundle args = new Bundle();
        args.putAll(oneFragmentTuple.m_fragmentArgsBundle) ;
        args.putInt(PlaceholderFragment.ARG_SECTION_NUMBER, oneFragmentTuple.m_sectionNumber);
        fragmentToReturn.setArguments(args);
        return fragmentToReturn ;
    }

    @Override
    public int getCount()
    {
        return this.m_fragmentsToHandleHashMap.size() ;
    }

    public CharSequence getPageTitle(int position)
    {
        SSRGenericFragmentPagerAdapterTuple oneTuple = this.m_fragmentsToHandleHashMap.get(position) ;
        return oneTuple.m_title ;
    }
}
