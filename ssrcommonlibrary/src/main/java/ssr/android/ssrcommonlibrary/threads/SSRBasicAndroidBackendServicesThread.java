package ssr.android.ssrcommonlibrary.threads;

import android.util.Log;

import java.util.HashMap;

import ssrcommon.threads.ISSRBackendRequest;
import ssrcommon.threads.ISSRBackendResponse;
import ssrcommon.threads.SSRBasicBackendServicesThread;

/**
 * Created on 1/6/2017.
 * Purpose : This class will be the "basic" SSRThread version for Android platform.
 */
public class SSRBasicAndroidBackendServicesThread extends SSRBasicBackendServicesThread
{
    public SSRBasicAndroidBackendServicesThread(ISSRBackendRequest i_requestToBackend,
                                                ISSRBackendResponse o_responseFromBackend)
            throws Exception
    {
        super(i_requestToBackend, o_responseFromBackend, null) ;
    }

    public SSRBasicAndroidBackendServicesThread(ISSRBackendRequest i_requestToBackend,
                                                ISSRBackendResponse o_responseFromBackend,
                                                HashMap<Object, Object> i_parametersHashMap)
            throws Exception
    {
        // Instantiate the object with same parameters, and default timeout value of 10 seconds.
        super(i_requestToBackend, o_responseFromBackend, i_parametersHashMap, 10000) ;
    }

    public SSRBasicAndroidBackendServicesThread(ISSRBackendRequest i_requestToBackend,
                                                ISSRBackendResponse o_responseFromBackend,
                                                HashMap<Object, Object> i_parametersHashMap,
                                                Integer i_socketTimeoutToSet)
            throws Exception
    {
        super(i_requestToBackend, o_responseFromBackend, i_parametersHashMap, i_socketTimeoutToSet) ;
    }

    @Override
    protected void logMessage(String i_logMessage, Object i_auxiliaryParameter)
    {
        if(true == i_auxiliaryParameter instanceof Exception)
        {
            Log.e("ssrana", i_logMessage, (Exception)i_auxiliaryParameter) ;
        }
        else
        {
            Log.e("ssrana", i_logMessage);
        }
    }
}
