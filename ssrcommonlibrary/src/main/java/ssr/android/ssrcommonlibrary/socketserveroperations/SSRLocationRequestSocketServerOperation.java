package ssr.android.ssrcommonlibrary.socketserveroperations;

import android.location.Location;
import android.util.Log;

import ssr.android.ssrcommonlibrary.services.ISSRLocationServiceBoundInterface;
import ssr.android.ssrcommonlibrary.threads.SSRSocketP2PMessagePosterThreadForAndroid;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import sockettunnelling.entities.MessageContainer;
import sockettunnelling.entities.SendMessageToPeerRequestData;
import sockettunnelling.threads.SSRSocketP2PCommunicationManagerThread;
import ssrcommon.design.ISSRKeyControlledCallbackInterface;
import ssrcommon.design.SSRKeyControlledCallbackGateKeeper;
import ssrcommon.exceptions.SSRBasicException;

/**
 * Created by Siddhartha on 5/17/2017.
 * Purpose : This class implements socket server operation for fetching location of the client
 *           and sending it back, via the socket server, to the one who requested the location.
 *           This class will both receive the request and handle the response. When an app wants
 *           to use this operation, they will have to derive from it and put handling logic in
 *           the relevant methods to implement their app logic.
 */
public class SSRLocationRequestSocketServerOperation implements ISSRKeyControlledCallbackInterface
{
    public static final String IDENTIFIER_STRING = "SSRLocationRequestSocketServerOperation_IDENTIFIER_STRING" ;
    public static final String LOCATION_REQUEST_KEY = "LOCATION_REQUEST_KEY" ;
    public static final String LOCATION_RESPONSE_KEY = "LOCATION_RESPONSE_KEY" ;
    public static final String RESPONSE_LATITUDE_KEY = "RESPONSE_LATITUDE_KEY" ;
    public static final String RESPONSE_LONGITUDE_KEY = "RESPONSE_LONGITUDE_KEY" ;
    public static final String RESPONSE_TIMESTAMP_KEY = "RESPONSE_TIMESTAMP_KEY" ;

    protected static SSRLocationRequestSocketServerOperation ms_uniqueInstance ;

    protected ISSRLocationServiceBoundInterface m_locationServiceBoundInterface ;
    protected String m_selfClientId ;

    public static SSRLocationRequestSocketServerOperation getUniqueInstance() throws Exception
    {
        SSRLocationRequestSocketServerOperation toReturn = null ;
        if(null == SSRLocationRequestSocketServerOperation.ms_uniqueInstance)
        {
            throw new SSRBasicException("getUniqueInstance was called for "
                    + "SSRSocketP2PCommunicationManagerThread without setting unique instance.") ;
        }
        else
        {
            toReturn = SSRLocationRequestSocketServerOperation.ms_uniqueInstance ;
        }
        return toReturn ;
    }

    public static synchronized void setUniqueInstance(SSRLocationRequestSocketServerOperation
                                                              i_uniqueInstanceToStore, Boolean i_clobber)
            throws Exception
    {
        if(true == i_clobber || null == SSRLocationRequestSocketServerOperation.ms_uniqueInstance)
        {
            SSRLocationRequestSocketServerOperation.ms_uniqueInstance = i_uniqueInstanceToStore ;
        }
        else
        {
            throw new SSRBasicException("SSRSocketP2PCommunicationManagerThread.setUniqueInstance "
                    + "is being reset.") ;
        }
    }

    // For sake of simplicity, since this is the first implementation, the location binder
    // will have to be provided to this operation during the time of registration, and it will
    // check by calling for the location of the device, here itself.
    public SSRLocationRequestSocketServerOperation(ISSRLocationServiceBoundInterface i_interfaceForLocation,
                                                   String i_clientIdToStore) throws Exception
    {
        this.m_locationServiceBoundInterface = i_interfaceForLocation ;
        this.m_selfClientId = i_clientIdToStore ;
        Map.Entry<GregorianCalendar, Location> oneLocation = this.m_locationServiceBoundInterface
                .getLastLocationDetails() ;
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss") ;
        String dateToPrint = dateFormat.format(oneLocation.getKey().getTime()) ;
        Log.e("ssrana", "SSRLocationRequestSocketServerOperation initialization location "
                + "\ntimestamp : " + dateToPrint
                + "\nLatitude : " + oneLocation.getValue().getLatitude()
                + "\nLongitude : " + oneLocation.getValue().getLongitude()) ;
    }

    public static void sendLocationRequest(String i_sourceClient, String i_destinationClient)
        throws Exception
    {
        // Create response MessageData
        HashMap<String, Serializable> responseMessageData = new HashMap<String, Serializable>() ;
        responseMessageData.put(MessageContainer.REQUESTED_OPERATION_PARAM,
                SSRLocationRequestSocketServerOperation.LOCATION_REQUEST_KEY) ;

        // Create request MessageContainer
        MessageContainer requestContainer = new MessageContainer() ;
        requestContainer.setSourcePeerId(i_sourceClient) ;
        requestContainer.setMessageType(MessageContainer.MessageType.OPERATION) ;
        requestContainer.setMessageData(responseMessageData);

        // Create request object
        SendMessageToPeerRequestData locationRequestData = new SendMessageToPeerRequestData() ;
        locationRequestData.setSourcePeerId(i_sourceClient) ;
        locationRequestData.setDestinationPeerId(i_destinationClient) ;
        locationRequestData.setMessageData(requestContainer);

        SSRSocketP2PMessagePosterThreadForAndroid locationRequestPosterThread =
                new SSRSocketP2PMessagePosterThreadForAndroid(locationRequestData) ;
        locationRequestPosterThread.run() ;
    }

    // This method is just a "programming sugar". The use could have handled registration himself, but
    // just in case out of laziness he decides not to do that, this is the standard implementation for
    // the registration.
    public void handleOperationRegistration(SSRSocketP2PCommunicationManagerThread i_socketCommunicationManagerThread)
            throws Exception
    {
        // Register location request and response with the socket P2P handler thread
        i_socketCommunicationManagerThread.registerOperation(SSRLocationRequestSocketServerOperation.LOCATION_REQUEST_KEY,
                SSRLocationRequestSocketServerOperation.IDENTIFIER_STRING,
                SSRLocationRequestSocketServerOperation.LOCATION_REQUEST_KEY) ;
        i_socketCommunicationManagerThread.registerOperation(SSRLocationRequestSocketServerOperation.LOCATION_RESPONSE_KEY,
                SSRLocationRequestSocketServerOperation.IDENTIFIER_STRING,
                SSRLocationRequestSocketServerOperation.LOCATION_RESPONSE_KEY) ;

        // Register callback with the callback gatekeeper
        SSRKeyControlledCallbackGateKeeper.getInstance().registerCallback(
                SSRLocationRequestSocketServerOperation.IDENTIFIER_STRING, this) ;
    }

    @Override
    public Boolean checkKeyFits(Object i_keyObject) throws Exception
    {
        if(false == SSRLocationRequestSocketServerOperation.LOCATION_REQUEST_KEY.equals(i_keyObject.toString())
                && false == SSRLocationRequestSocketServerOperation.LOCATION_RESPONSE_KEY.equals(i_keyObject.toString()))
        {
            // if the key doesn't match request and the key doesn't match response key strings,
            // then the key is not fitting.
            return false ;
        }
        else
        {
            return true ;
        }
    }

    @Override
    public Object executeCallback(Object i_keyObject, Object i_paramsObject) throws Exception
    {
        // For now we are going to assume that there is no invalid input coming to the operation
        Log.e("ssrana", "SSRLocationRequestSocketServerOperation callback received input :"
                + "\nKey: " + i_keyObject.toString() + "\nParams: " + i_paramsObject.toString()) ; ;
        if(SSRLocationRequestSocketServerOperation.LOCATION_REQUEST_KEY.equals(i_keyObject.toString()))
        {
            return this.handleLocationRequest(i_keyObject, i_paramsObject);
        }
        else if(SSRLocationRequestSocketServerOperation.LOCATION_RESPONSE_KEY.equals(i_keyObject.toString()))
        {
            return this.handleLocationResponse(i_keyObject, i_paramsObject) ;
        }
        return null;
    }

    public Object handleLocationRequest(Object i_keyObject, Object i_paramObject) throws Exception
    {
        Object toReturn = null ;

        // Default implementation is to just get the location and send it as message back to the
        // client that had requested the location.

        MessageContainer locationRequestMessage = (MessageContainer)i_paramObject ;
        Map.Entry<GregorianCalendar, Location> oneLocation = this.m_locationServiceBoundInterface
                .getLastLocationDetails() ;

        // Create response MessageData
        HashMap<String, Serializable> responseMessageData = new HashMap<String, Serializable>() ;
        responseMessageData.put(MessageContainer.REQUESTED_OPERATION_PARAM,
                SSRLocationRequestSocketServerOperation.LOCATION_RESPONSE_KEY) ;
        responseMessageData.put(SSRLocationRequestSocketServerOperation.RESPONSE_LATITUDE_KEY,
                oneLocation.getValue().getLatitude()) ;
        responseMessageData.put(SSRLocationRequestSocketServerOperation.RESPONSE_LONGITUDE_KEY,
                oneLocation.getValue().getLongitude()) ;
        responseMessageData.put(SSRLocationRequestSocketServerOperation.RESPONSE_TIMESTAMP_KEY,
                oneLocation.getKey()) ;

        // Create response MessageContainer
        MessageContainer responseContainer = new MessageContainer() ;
        responseContainer.setSourcePeerId(this.m_selfClientId) ;
        responseContainer.setMessageType(MessageContainer.MessageType.OPERATION) ;
        responseContainer.setMessageData(responseMessageData);

        // Create response object
        SendMessageToPeerRequestData locationResponseData = new SendMessageToPeerRequestData() ;
        locationResponseData.setSourcePeerId(this.m_selfClientId) ;
        locationResponseData.setDestinationPeerId(locationRequestMessage.getSourcePeerId()) ;
        locationResponseData.setMessageData(responseContainer);

        SSRSocketP2PMessagePosterThreadForAndroid locationResponsePosterThread =
                new SSRSocketP2PMessagePosterThreadForAndroid(locationResponseData) ;
        locationResponsePosterThread.run() ;

        return toReturn ;
    }

    public Object handleLocationResponse(Object i_keyObject, Object i_paramObject) throws Exception
    {
        Object toReturn = null ;

        // Default implementation is to just show in the log, the location that was received as
        // response, from the remote client.

        String clientId = this.getClientIdAtLocation(i_paramObject) ;
        Map.Entry<GregorianCalendar, Location> responseLocation = this.getLocationDetailsFromResponse(i_paramObject) ;
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss") ;
        String dateToPrint = dateFormat.format(responseLocation.getKey().getTime()) ;
        Log.e("ssrana", "SSRLocationRequestSocketServerOperation location response "
                + "\nclientId : " + clientId
                + "\ntimestamp : " + dateToPrint
                + "\nLatitude : " + responseLocation.getValue().getLatitude()
                + "\nLongitude : " + responseLocation.getValue().getLongitude()) ;

        return toReturn ;
    }

    // This is just a programmatic sugar thing. There is no necessity of this method to exist
    protected Map.Entry<GregorianCalendar, Location> getLocationDetailsFromResponse(
            Object i_messageContainerAsObject) throws Exception
    {
        Map.Entry<GregorianCalendar, Location> toReturn = null ;
        MessageContainer locationResponseMessage = (MessageContainer)i_messageContainerAsObject ;
        HashMap<String, Serializable> responseMessageData = (HashMap<String, Serializable>)
                locationResponseMessage.getMessageData() ;

        Double responseLatitude = (Double)responseMessageData.get(SSRLocationRequestSocketServerOperation
                .RESPONSE_LATITUDE_KEY) ;
        Double responseLongitude = (Double)responseMessageData.get(SSRLocationRequestSocketServerOperation
                .RESPONSE_LONGITUDE_KEY) ;
        GregorianCalendar responseTimeStamp = (GregorianCalendar)responseMessageData.get(
                SSRLocationRequestSocketServerOperation.RESPONSE_TIMESTAMP_KEY) ;

        Location responseLocation = new Location("") ;
        responseLocation.setLatitude(responseLatitude) ;
        responseLocation.setLongitude(responseLongitude) ;
        toReturn = new AbstractMap.SimpleEntry<GregorianCalendar, Location>(responseTimeStamp, responseLocation) ;
        return toReturn ;
    }

    // This is just a programmatic sugar thing. There is no necessity of this method to exist
    protected String getClientIdAtLocation(Object i_messageContainerAsObject) throws Exception
    {
        MessageContainer locationResponseMessage = (MessageContainer)i_messageContainerAsObject ;
        return locationResponseMessage.getSourcePeerId() ;
    }
}
