package ssr.android.ssrcommonlibrary.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import ssr.android.ssrcommonlibrary.customviews.SSRMixedListRow;

import java.util.HashMap;
import java.util.Vector;

/**
 * Created on 8/11/2016.
 */
public class SSRMixedListArrayAdapter extends ArrayAdapter<Object> implements AdapterView.OnItemClickListener, View.OnClickListener
{
    public Activity m_activityToStore ;
    public Vector<SSRMixedListRow> m_rowsToManage ;
    public String m_adapterIdentifier ;

    public SSRMixedListArrayAdapter(Activity i_activityContext, int i_resource, int i_textViewResourceId,
                                    Vector<SSRMixedListRow> i_rowsVector)
    {
        super(i_activityContext, i_resource, i_textViewResourceId, i_rowsVector.toArray()) ;

        this.m_activityToStore = i_activityContext ;
        this.m_rowsToManage = i_rowsVector ;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        // empty implementation
    }

    @Override
    public View getView(int i_position, View i_convertView, ViewGroup i_parent)
    {
        View viewToReturn = null ;
        try
        {
            LayoutInflater layoutInflaterToUse = this.m_activityToStore.getLayoutInflater();
            SSRMixedListRow rowToCreateViewFor = this.m_rowsToManage.get(i_position);
            viewToReturn = rowToCreateViewFor.getViewToShow(layoutInflaterToUse, i_parent);
            //viewToReturn.setTag(R.integer.view_context_identifier_tag_key, this.m_adapterIdentifier) ;
        }
        catch(Exception e)
        {
            Log.e("ssrana", "Exception caught in SSRMixedListArrayAdapter.getView :\n", e) ;
        }
        return viewToReturn ;
    }

    public HashMap<String, Object> getInputRowResults()
    {
        HashMap<String, Object> toReturn = new HashMap<String, Object>() ;
        try
        {
            for (int rowsIterator = 0; rowsIterator < this.m_rowsToManage.size(); rowsIterator++)
            {
                SSRMixedListRow oneRow = this.m_rowsToManage.get(rowsIterator);
                oneRow.putResult(toReturn);
            }
        }
        catch(Exception e)
        {
            Log.e("ssrana", "Exception caught in SSRMixedListArrayAdapter.getInputRowResults :\n", e) ;
        }
        return toReturn ;
    }

    @Override
    public void onClick(View v)
    {
        // Empty implementation
    }
}
