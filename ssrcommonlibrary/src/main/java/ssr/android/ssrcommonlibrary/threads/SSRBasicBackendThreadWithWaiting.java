package ssr.android.ssrcommonlibrary.threads;

import android.content.Context;

import ssr.android.ssrcommonlibrary.SSRFunctionalityLibrary;

import java.util.HashMap;

import ssrcommon.threads.ISSRBackendRequest;
import ssrcommon.threads.ISSRBackendResponse;

/**
 * Created on 12/19/2016.
 * Purpose : This class just adds the waiting dialog functionality to the
 */
public class SSRBasicBackendThreadWithWaiting extends SSRBasicAndroidBackendServicesThread
{
    public static final String WAITING_DIALOG_INTENT_PARAMETER_NAME = "WAITING_DIALOG_INTENT_PARAMETER" ;
    public static final String WAITING_DIALOG_CALLBACK_PARAMETER_NAME = "WAITING_DIALOG_CALLBACK_PARAMETER" ;
    public static final String WAITING_DIALOG_CALLBACKWITHARGS_PARAMETER_NAME = "WAITING_DIALOG_CALLBACKWITHARGS_PARAMETER" ;

    public Context m_contextForWaitingDialog ;
    public String m_waitingDialogMessage ;

    public SSRBasicBackendThreadWithWaiting(ISSRBackendRequest i_requestToBackend,
                                            ISSRBackendResponse o_responseFromBackend,
                                            HashMap<Object, Object> i_parametersHashMap,
                                            Context i_contextForWaitingDialog,
                                            String i_waitingDialogMessage)
            throws Exception
    {
        // Instantiate object with default socket timeout of 10 seconds.
        this(i_requestToBackend, o_responseFromBackend, i_parametersHashMap, i_contextForWaitingDialog,
                i_waitingDialogMessage, 10000) ;
    }

    public SSRBasicBackendThreadWithWaiting(ISSRBackendRequest i_requestToBackend,
                                            ISSRBackendResponse o_responseFromBackend,
                                            HashMap<Object, Object> i_parametersHashMap,
                                            Context i_contextForWaitingDialog,
                                            String i_waitingDialogMessage,
                                            Integer i_socketTimeoutToSet)
        throws Exception
    {
        super(i_requestToBackend, o_responseFromBackend, i_parametersHashMap, i_socketTimeoutToSet) ;
        this.m_contextForWaitingDialog = i_contextForWaitingDialog ;
        this.m_waitingDialogMessage = i_waitingDialogMessage ;
    }

    @Override
    public void run()
    {
        // TODO : Figure out why the logMessage call below is not working if the second argument is not given
        this.logMessage("SSRBasicBackendThreadWithWaiting version 1.1", null) ;
        Boolean waitingDialogCreated = false ;
        try
        {
            this.showWaitingDialog() ;
            waitingDialogCreated = true ;
            super.run() ;
        }
        catch(Exception e)
        {
            this.handleThreadFlowException("Exception caught in SSRBasicBackendThreadWithWaiting.run (1)", e) ;
        }
        finally
        {
            try
            {
                if(true == waitingDialogCreated)
                    this.dismissWaitingDialog() ;
            }
            catch(Exception e)
            {
                this.handleThreadFlowException("Exception caught in SSRBasicBackendThreadWithWaiting.run (2)", e) ;
            }
        }
    }

    public void showWaitingDialog() throws Exception
    {
        String logMessage = "Entering SSRBasicBackendThreadWithWaiting.showWaitingDialog" ;
        this.logMessage(logMessage, null) ;
        this.logMessage("Creating the waiting dialog", null) ;
        SSRFunctionalityLibrary.showWaitingDialog(this.m_contextForWaitingDialog, this.m_waitingDialogMessage) ;
    }

    public void dismissWaitingDialog() throws Exception
    {
        String logMessage = "Entering SSRBasicBackendThreadWithWaiting.dismissWaitingDialog" ;
        this.logMessage(logMessage, null) ;

        Object optionalParameterToSend = null ;

        if(true == this.m_parametersHashMap.containsKey(SSRBasicBackendThreadWithWaiting
                .WAITING_DIALOG_INTENT_PARAMETER_NAME))
        {
            this.logMessage("Optional parameter Intent found in SSRBasicBackendThreadWIthWaiting.dismissWaitingDialog", null) ;
            optionalParameterToSend = this.m_parametersHashMap.get(SSRBasicBackendThreadWithWaiting
                    .WAITING_DIALOG_INTENT_PARAMETER_NAME) ;
        }
        else if(true == this.m_parametersHashMap.containsKey(SSRBasicBackendThreadWithWaiting
                .WAITING_DIALOG_CALLBACK_PARAMETER_NAME))
        {
            this.logMessage("Optional parameter callback found in SSRBasicBackendThreadWithWaiting.dismissWaitingDialog", null) ;
            optionalParameterToSend = this.m_parametersHashMap.get(SSRBasicBackendThreadWithWaiting
                    .WAITING_DIALOG_CALLBACK_PARAMETER_NAME) ;
        }
        else if(true == this.m_parametersHashMap.containsKey(SSRBasicBackendThreadWithWaiting
                .WAITING_DIALOG_CALLBACKWITHARGS_PARAMETER_NAME))
        {
            this.logMessage("Optional parameter callback with arguments found in SSRBasicBackendThreadWithWaiting.dismissWaitingDialog", null) ;
            Object callbackObject = this.m_parametersHashMap.get(SSRBasicBackendThreadWithWaiting
                    .WAITING_DIALOG_CALLBACKWITHARGS_PARAMETER_NAME) ;
            optionalParameterToSend = new Object[] {callbackObject, this.m_requestToBackend,
                    this.m_responseFromBackend} ;
        }

        this.logMessage("Dismissing waiting dialog.", null) ;
        SSRFunctionalityLibrary.dismissWaitingDialog(optionalParameterToSend) ;
    }
}
