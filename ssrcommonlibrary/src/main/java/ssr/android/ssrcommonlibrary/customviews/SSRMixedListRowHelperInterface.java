package ssr.android.ssrcommonlibrary.customviews;

import android.view.LayoutInflater;
import android.view.ViewGroup;

/**
 * Created on 8/23/2016.
 */
public interface SSRMixedListRowHelperInterface
{
    public void onCreateHandling(SSRMixedListRow i_rowToHandleOnCreate, LayoutInflater i_layoutInflaterToUse,
                                 ViewGroup i_parentView) ;
}
