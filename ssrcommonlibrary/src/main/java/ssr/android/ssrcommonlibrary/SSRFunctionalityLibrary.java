package ssr.android.ssrcommonlibrary;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;

import ssr.android.ssrcommonlibrary.entities.ContactData;
import ssr.android.ssrcommonlibrary.handlers.AlertDialogHandlerForUIThread;
import ssr.android.ssrcommonlibrary.handlers.ShowToastHandler;
import ssr.android.ssrcommonlibrary.handlers.WaitingDialogHandlerForUIThread;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import ssrcommon.exceptions.SSRBasicException;

/**
 * Created on 12/4/2016.
 * Purpose : This class will contain the general functionalities, without storing any context,
 *           that are needed when coding in android.
 */
public class SSRFunctionalityLibrary
{
    protected static HashMap<Long, HashMap<String, Handler>> m_sHashMapForUIHandlersPerThread ;

    static
    {
        SSRFunctionalityLibrary.m_sHashMapForUIHandlersPerThread =
                new HashMap<Long, HashMap<String, Handler>>() ;
    }

    public static ProgressDialog createAndReturnWaitingDialog(Context i_contextToUse)
    {
        ProgressDialog waitingDialogForUIThread = null ;
        if(Looper.myLooper() == Looper.getMainLooper())
        {
            waitingDialogForUIThread = new ProgressDialog(i_contextToUse) ;
            waitingDialogForUIThread.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            waitingDialogForUIThread.setMessage("Loading. Please wait...");
            waitingDialogForUIThread.setIndeterminate(true);
            waitingDialogForUIThread.setCanceledOnTouchOutside(false);
        }
        return waitingDialogForUIThread ;
    }

    // Eventually the code has to be changed to use createHandler method. Until that
    // time, make sure that the code of these two methods are in synch
    public static void createUniversalHandlers() throws Exception
    {
        if(Looper.getMainLooper() == Looper.myLooper())
        {
            HashMap<String, Handler> currentThreadHashMap = SSRFunctionalityLibrary
                    .m_sHashMapForUIHandlersPerThread.get(Thread.currentThread().getId()) ;
            if(null == currentThreadHashMap)
            {
                currentThreadHashMap = new HashMap<String, Handler>() ;
                SSRFunctionalityLibrary.m_sHashMapForUIHandlersPerThread.put(Thread.currentThread()
                        .getId(), currentThreadHashMap) ;
            }
            // Create and store waiting dialog handler
            if (null == currentThreadHashMap.get(WaitingDialogHandlerForUIThread.WAITING_DIALOG_HANDLE_NAME))
            {
                Handler waitingDialogHandler = new WaitingDialogHandlerForUIThread();
                currentThreadHashMap.put(WaitingDialogHandlerForUIThread.WAITING_DIALOG_HANDLE_NAME, waitingDialogHandler);
            }

            // Create and store toast showing handler
            if (null == currentThreadHashMap.get(ShowToastHandler.SHOW_TOAST_HANDLER_NAME))
            {
                Handler showToastHandler = new ShowToastHandler(Looper.getMainLooper());
                currentThreadHashMap.put(ShowToastHandler.SHOW_TOAST_HANDLER_NAME, showToastHandler);
            }

            // Create and store alert dialog handler
            if (null == currentThreadHashMap.get(AlertDialogHandlerForUIThread.ALERT_DIALOG_HANDLE_NAME))
            {
                Handler alertDialogHandler = new AlertDialogHandlerForUIThread();
                currentThreadHashMap.put(AlertDialogHandlerForUIThread.ALERT_DIALOG_HANDLE_NAME, alertDialogHandler);
            }
        }
    }

    // Eventually the code in createUniversalHandlers has to be changed to use this method. Until that
    // time, make sure that the code of these two methods are in synch
    public static void createHandler(String i_handlerName, Handler i_handlerToStore) throws Exception
    {
        // If the operation gets called from a non gui thread, then we will throw exception
        if(Looper.getMainLooper() != Looper.myLooper())
        {
            throw new SSRBasicException("Handler creation flow called from a non-gui thread.") ;
        }
        else
        {
            HashMap<String, Handler> currentThreadHashMap = SSRFunctionalityLibrary
                    .m_sHashMapForUIHandlersPerThread.get(Thread.currentThread().getId()) ;
            if(null == currentThreadHashMap)
            {
                currentThreadHashMap = new HashMap<String, Handler>() ;
                SSRFunctionalityLibrary.m_sHashMapForUIHandlersPerThread.put(Thread.currentThread()
                        .getId(), currentThreadHashMap) ;
            }
            // Store handler
            if (null == currentThreadHashMap.get(i_handlerName))
            {
                currentThreadHashMap.put(i_handlerName, i_handlerToStore);
            }
        }
    }

    public static void showWaitingDialog(Context i_contextToUse, String i_stringToShow)
            throws Exception
    {
        Object [] parametersForWaitingDialogHandler = new Object[] {i_contextToUse,
                i_stringToShow} ;
        Message messageToShowWaitingDialog = SSRFunctionalityLibrary.m_sHashMapForUIHandlersPerThread
                .get(Looper.getMainLooper().getThread().getId()).get(WaitingDialogHandlerForUIThread
                        .WAITING_DIALOG_HANDLE_NAME).obtainMessage(
                        WaitingDialogHandlerForUIThread.WAITING_DIALOG_SHOW, parametersForWaitingDialogHandler) ;
        messageToShowWaitingDialog.sendToTarget() ;
    }

    public static void showAlertDialog(Context i_contextToUse, String i_alertTitle, String i_stringToShow)
            throws Exception
    {
        Object [] parametersForAlertDialogHandler = new Object[] {i_contextToUse,
                i_alertTitle, i_stringToShow} ;
        Message messageToShowAlertDialog = SSRFunctionalityLibrary.m_sHashMapForUIHandlersPerThread
                .get(Looper.getMainLooper().getThread().getId()).get(AlertDialogHandlerForUIThread
                        .ALERT_DIALOG_HANDLE_NAME).obtainMessage(0, parametersForAlertDialogHandler) ;
        messageToShowAlertDialog.sendToTarget() ;
    }

    public static void dismissWaitingDialog() throws Exception
    {
        /*Message messageToDismissWaitingDialog = SSRFunctionalityLibrary.m_sHashMapForUIHandlersPerThread
                .get(Looper.getMainLooper().getThread().getId()).get(WaitingDialogHandlerForUIThread
                        .WAITING_DIALOG_HANDLE_NAME).obtainMessage(WaitingDialogHandlerForUIThread
                        .WAITING_DIALOG_DISMISS) ;
        messageToDismissWaitingDialog.sendToTarget() ;*/
        SSRFunctionalityLibrary.dismissWaitingDialog(null) ;
    }

    public static void dismissWaitingDialog(Object i_objectForMessage) throws Exception
    {
        Message messageToDismissWaitingDialog = SSRFunctionalityLibrary.m_sHashMapForUIHandlersPerThread
                .get(Looper.getMainLooper().getThread().getId()).get(WaitingDialogHandlerForUIThread
                        .WAITING_DIALOG_HANDLE_NAME).obtainMessage(WaitingDialogHandlerForUIThread
                        .WAITING_DIALOG_DISMISS) ;
        if(null != i_objectForMessage)
        {
            messageToDismissWaitingDialog.obj = i_objectForMessage;
        }
        messageToDismissWaitingDialog.sendToTarget() ;
    }

    public static void showToast(Context i_contextToUse, String i_textToShow)
            throws Exception
    {
        Object[] parametersForShowToastHandler = new Object[]{i_contextToUse, i_textToShow};
        Message showToastMessage = SSRFunctionalityLibrary.m_sHashMapForUIHandlersPerThread
                .get(Looper.getMainLooper().getThread().getId()).get(ShowToastHandler
                        .SHOW_TOAST_HANDLER_NAME).obtainMessage(0, parametersForShowToastHandler);
        showToastMessage.sendToTarget();
    }

    public static String fetchSelfImei(Context i_contextToUse)
    {
        TelephonyManager telephonyManager = (TelephonyManager)i_contextToUse.getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId() ;
    }

    public static void getPhoneContactsAsContactDataHashMap(ContentResolver i_contentResolver,
                                                            HashMap<String, ContactData> o_phoneContactsDataToReturn)
    {
        HashMap<String, ContactData> mapOfEmergencyContactsDataWithNumberKey =
                new HashMap<String, ContactData>() ;
        SSRFunctionalityLibrary.getPhoneContactsAsContactDataHashMapWithNumberKey(
                i_contentResolver, mapOfEmergencyContactsDataWithNumberKey) ;
        Iterator<ContactData> phoneContactsIterator = mapOfEmergencyContactsDataWithNumberKey.values().iterator() ;
        while(true == phoneContactsIterator.hasNext())
        {
            ContactData onePhoneContactAsEmergencyContactsData = phoneContactsIterator.next() ;

            o_phoneContactsDataToReturn.put(onePhoneContactAsEmergencyContactsData.getName(),
                    onePhoneContactAsEmergencyContactsData) ;
        }
    }

    public static void getPhoneContactsAsContactDataHashMapWithNumberKey(
            ContentResolver i_contentResolver,
            HashMap<String, ContactData> o_phoneContactsDataToReturn)
    {
        Cursor contentsCursor = i_contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null) ;
        if(contentsCursor.getCount() > 0)
        {
            // TODO : Put functionality to handle ppl having multiple phone numbers.
            while(contentsCursor.moveToNext())
            {
                ContactData oneContactData = new ContactData() ;
                oneContactData.setName(contentsCursor.getString(
                        contentsCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME))) ;
                oneContactData.setHasPhoneNumber(contentsCursor.getString(
                        contentsCursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) ;
                if (oneContactData.getHasPhoneNumber().endsWith("0"))
                {
                    continue ;
                }

                Vector<String> contactPhoneNumbers =
                        SSRFunctionalityLibrary.getPhoneNumbersFromContactId(
                                i_contentResolver,
                                contentsCursor.getString(
                                        contentsCursor.getColumnIndex(ContactsContract.Contacts._ID))) ;
                // TODO : Put handling for multiple phone numbers
                oneContactData.setPhoneNumber(contactPhoneNumbers.size() > 0 ?
                        contactPhoneNumbers.firstElement() : "") ;
                // TODO : Have to write proper code to create URI using proper phone number.
                // Place holder code here. To be removed when actual code is put in place.
                /*oneContactData.m_uri = contentsCursor.getString(
                        contentsCursor.getColumnIndex(ContactsContract.Contacts.CONTENT_URI)) ;*/

                o_phoneContactsDataToReturn.put(oneContactData.getPhoneNumber(), oneContactData) ;
            }
        }
    }

    public static Vector<String> getPhoneNumbersFromContactId(ContentResolver i_contentResolver, String i_contactId)
    {
        Vector<String> phoneNumberVectorToReturn = new Vector<String>() ;
        String oneNumber = "";
        // **** DO NOTICE, THE COMMENTED CLAUSE. When using _ID phone number was not being retrieved using
        // **** the contact id as bind value. But when we used CONTACT_ID instead of _ID, phone number was
        // **** being retrieved using the contact id
        Cursor phones = i_contentResolver.query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null,
                /*ContactsContract.CommonDataKinds.Phone._ID*/ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                new String[]{i_contactId},
                null);

        if(phones.getCount() > 0)
        {
            while(phones.moveToNext())
            {
                // Due to formatting technique used, country code is truncated right now and hence
                // choosing phone number from another country will not work.
                oneNumber = SSRFunctionalityLibrary.formatPhoneNumber(
                        phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
                if("" != oneNumber)
                    phoneNumberVectorToReturn.add(oneNumber) ;
            }
            // TODO : Put log message in case you think it is required.
        }

        phones.close();

        return phoneNumberVectorToReturn ;
    }

    public static String formatPhoneNumber(String i_phoneNumberToFormat)
    {
        // For now, we are going to format the phone number to have only last 10 digits
        // The plus symbol '+' is not being removed right now because the assumption is
        // that if the phone number has a '+' character, it will anyways be more than 10
        // digits.

        // Remove the useless characters
        String toReturn = i_phoneNumberToFormat ;
        toReturn = toReturn.replaceAll(" ", "") ;
        toReturn = toReturn.replaceAll("\\(", "") ;
        toReturn = toReturn.replaceAll("\\)", "") ;
        toReturn = toReturn.replaceAll("-", "") ;


        // If phone number size is too short, no need of doing any processing
        int lengthOfPhoneNumberString = toReturn.length() ;
        if(10 > lengthOfPhoneNumberString)
        {
            // Phone number too short
            return "" ;
        }

        toReturn = toReturn.substring(lengthOfPhoneNumberString - 10, lengthOfPhoneNumberString) ;
        return toReturn ;
    }
}
