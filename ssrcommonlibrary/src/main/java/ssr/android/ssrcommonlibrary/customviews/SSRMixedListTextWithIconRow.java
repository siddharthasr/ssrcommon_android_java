package ssr.android.ssrcommonlibrary.customviews;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ssr.android.ssrcommonlibrary.R;

/**
 * Created on 8/12/2016.
 */
public class SSRMixedListTextWithIconRow extends SSRMixedListRow
{
    public Integer m_imageResourceIdForIcon ;
    public Integer m_textViewResourceId ;
    public ImageView m_iconView ;
    public TextView m_textView ;

    public SSRMixedListTextWithIconRow() throws Exception
    {
        super() ;
    }

    public SSRMixedListTextWithIconRow(String i_rowTextToStore, Integer i_iconResourceIdToStore,
                                       Integer i_textResourceIdToStore) throws Exception
    {
        super(i_rowTextToStore) ;
        this.m_imageResourceIdForIcon = i_iconResourceIdToStore ;
        this.m_textViewResourceId = i_textResourceIdToStore ;
    }

    // It is to be noted, that the onClickHandler for the view has to be set when the view is
    // created and returned by the method below.
    @Override
    public View getViewToShow(LayoutInflater i_inflaterToUse, ViewGroup i_parentForView)
            throws Exception
    {
        View mainLayoutView = super.getViewToShow(i_inflaterToUse, i_parentForView) ;
        //viewToReturn.setOnClickListener(this) ;
        View viewToReturn = mainLayoutView.findViewById(R.id.mixedrows_textwithicon_layout) ;
        TextView textPart = ((TextView)viewToReturn.findViewById(R.id.mixedrows_textwithicon_text)) ;
        textPart.setText(this.m_listRowText) ;
        textPart.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                int id = v.getId();
            }
        }) ;
        ImageView iconPart = ((ImageView)viewToReturn.findViewById(R.id.mixedrows_textwithicon_icon)) ;
        iconPart.setImageResource(this.m_imageResourceIdForIcon) ;
        iconPart.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                int id = v.getId();
            }
        }) ;

        textPart.setFocusable(false) ;
        textPart.setFocusableInTouchMode(false) ;
        iconPart.setFocusable(false) ;
        iconPart.setFocusableInTouchMode(false) ;

        this.m_rowView = viewToReturn ;
        this.m_iconView = iconPart ;
        this.m_textView = textPart ;

        return viewToReturn ;
    }
}
