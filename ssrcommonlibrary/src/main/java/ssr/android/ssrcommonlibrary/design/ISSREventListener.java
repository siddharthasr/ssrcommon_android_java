package ssr.android.ssrcommonlibrary.design;

/**
 * Created on 1/29/2017.
 * Purpose : This will be the interface for the key controlled event listeners
 */
public interface ISSREventListener
{
    public void informEventOccurrence(Object i_event, Object i_eventParameters) ;

    public void eventDeRegistered(String i_eventIdentifier) ;
}
