package ssr.android.ssrcommonlibrary.design;

import android.util.Log;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import ssrcommon.SSROrderedMutexProtectedHashMap;
import ssrcommon.exceptions.SSRBasicException;

/**
 * Created on 1/29/2017.
 * Purpose : This class will be Handler / manager / gate keeper for event listeners and broadcasters
 */
public class SSREventsPatternManager
{
    protected static SSREventsPatternManager ms_uniqueInstance ;

    protected SSROrderedMutexProtectedHashMap<String, HashMap<String, ISSREventListener>> m_registeredEventListeners ;

    public static SSREventsPatternManager getInstance() throws Exception
    {
        SSREventsPatternManager toReturn = null ;
        if(null == SSREventsPatternManager.ms_uniqueInstance)
        {
            SSREventsPatternManager.createUniqueInstance() ;
            toReturn = SSREventsPatternManager.ms_uniqueInstance ;
        }
        else
        {
            toReturn = SSREventsPatternManager.ms_uniqueInstance ;
        }
        return toReturn ;
    }

    private static synchronized void createUniqueInstance() throws Exception
    {
        if(null == SSREventsPatternManager.ms_uniqueInstance)
        {
            SSREventsPatternManager.ms_uniqueInstance = new SSREventsPatternManager() ;
        }
    }

    protected SSREventsPatternManager() throws Exception
    {
        this.m_registeredEventListeners = new SSROrderedMutexProtectedHashMap<String, HashMap<String, ISSREventListener>>() ;
    }

    public void registerEvent(String i_eventIdentifier) throws Exception
    {
        if(true == this.m_registeredEventListeners.containsKey(i_eventIdentifier))
            throw new SSRBasicException("Event already registered.") ;

        HashMap<String, ISSREventListener> eventListeners = new HashMap<String, ISSREventListener>() ;
        this.m_registeredEventListeners.put(i_eventIdentifier, eventListeners) ;
    }

    public void deRegisterEvent(String i_eventIdentifier) throws Exception
    {
        if(false == this.m_registeredEventListeners.containsKey(i_eventIdentifier))
            throw new SSRBasicException("Event is not registered. Unknown event.") ;

        HashMap<String, ISSREventListener> eventListeners = this.m_registeredEventListeners.get(i_eventIdentifier) ;

        try
        {
            Iterator<ISSREventListener> eventListenerIterator = eventListeners.values().iterator() ;
            while(true == eventListenerIterator.hasNext())
            {
                ISSREventListener oneListener = eventListenerIterator.next() ;
                oneListener.eventDeRegistered(i_eventIdentifier) ;
                eventListenerIterator.remove() ;
            }
        }
        catch (Exception e)
        {
            Log.e("ssrana", "Exception caught in SSREventsPatternManager.deRegisterEventListener :\n", e) ;
        }
    }

    public void registerEventListener(String i_eventIdentifier, String i_listenerIdentifier,
                                      ISSREventListener i_eventListener) throws Exception
    {
        if(null == i_eventListener)
            throw new SSRBasicException("Event listener object is null") ;

        if(false == this.m_registeredEventListeners.containsKey(i_eventIdentifier))
            throw new SSRBasicException("Listener trying to register for unknown event") ;

        HashMap<String, ISSREventListener> eventListenersMap = this.m_registeredEventListeners.get(i_eventIdentifier) ;

        if(true == eventListenersMap.containsKey(i_listenerIdentifier))
            throw new SSRBasicException("Event listener is already registered") ;

        eventListenersMap.put(i_listenerIdentifier, i_eventListener) ;
    }

    public void deRegisterEventListener(String i_eventIdentifier, String i_listenerIdentifier) throws Exception
    {
        if(false == this.m_registeredEventListeners.containsKey(i_eventIdentifier))
            throw new SSRBasicException("Listener was not registered for event. Event itself is unknown.") ;

        HashMap<String, ISSREventListener> eventListeners = this.m_registeredEventListeners.get(i_eventIdentifier) ;

        if(false == eventListeners.containsKey(i_eventIdentifier))
            throw new SSRBasicException("Listener was not registered for event. Event is valid.") ;

        eventListeners.remove(i_eventIdentifier) ;
    }

    public void raiseEvent(String i_eventIdentifier, Object i_event, Object i_eventParameters) throws Exception
    {
        if(false == this.m_registeredEventListeners.containsKey(i_eventIdentifier))
            throw new SSRBasicException("Event is not registered.") ;

        HashMap<String, ISSREventListener> eventListeners = this.m_registeredEventListeners.get(i_eventIdentifier) ;
        this.informListenersOfRaisedEvent(eventListeners, i_event, i_eventParameters) ;
    }

    private void informListenersOfRaisedEvent(HashMap<String, ISSREventListener> i_eventListeners,
                                                Object i_event, Object i_eventParameters)
    {
        try
        {
            Iterator<Map.Entry<String, ISSREventListener>> listenersIterator = i_eventListeners.entrySet().iterator() ;
            while(true == listenersIterator.hasNext())
            {
                Map.Entry<String, ISSREventListener> oneEntry = listenersIterator.next() ;
                ISSREventListener oneEventListener = oneEntry.getValue() ;
                try
                {
                    oneEventListener.informEventOccurrence(i_event, i_eventParameters);
                }
                catch(Exception f)
                {
                    Log.e("ssrana", "Exception caught in SSREventsPatternManager.informListenersOfRaisedEvent "
                            + "while trying to inform event \"" + oneEntry.getKey() + "\" of raised event :\n", f) ;
                }
            }
        }
        catch (Exception e)
        {
            Log.e("ssrana", "Exception caught in SSREventsPatternManager.informListenersOfRaisedEvent :\n", e) ;
        }
    }
}
