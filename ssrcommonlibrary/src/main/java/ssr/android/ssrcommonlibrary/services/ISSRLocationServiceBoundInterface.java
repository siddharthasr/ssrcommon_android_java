package ssr.android.ssrcommonlibrary.services;

import android.location.Location;

import java.util.GregorianCalendar;
import java.util.Map;

/**
 * Created on 2/25/2017.
 * Purpose: This interface will provide the functionalities, that a service bound to SSRLocationService
 *          can get from SSRLocationService.
 */

// Since these are IPC / Remote calls, no exceptions must be thrown.

public interface ISSRLocationServiceBoundInterface
{
    public Map.Entry<GregorianCalendar, Location> getLastLocationDetails() ;
}
