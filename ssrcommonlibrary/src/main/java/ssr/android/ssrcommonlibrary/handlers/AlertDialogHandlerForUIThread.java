package ssr.android.ssrcommonlibrary.handlers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/**
 * Created on 8/27/2016.
 */
public class AlertDialogHandlerForUIThread extends Handler
{
    public static final String ALERT_DIALOG_HANDLE_NAME = "AlertDialogHandler" ;

    public AlertDialogHandlerForUIThread()
    {
        super(Looper.getMainLooper()) ;
    }

    public void handleMessage(Message inputMessage)
    {
        Object[] parameterObjectsArray = (Object[])inputMessage.obj ;
        Context contextToUse = (Context)parameterObjectsArray[0] ;
        String alertDialogTitle = (String) parameterObjectsArray[1] ;
        String messageToShow = (String) parameterObjectsArray[2] ;
        if (null == messageToShow)
            messageToShow = "No alert message passed." ;

        DialogInterface.OnClickListener alertDialogDismissButtonListener = new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int which)
            {
                dialog.dismiss() ;
            }
        } ;
        new AlertDialog.Builder(contextToUse)
                .setTitle(alertDialogTitle)
                .setMessage(messageToShow)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton("Ok", alertDialogDismissButtonListener)
                .show() ;
    }
}