package ssr.android.ssrcommonlibrary.services;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import ssr.android.ssrcommonlibrary.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.AbstractMap;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Map;

import ssrcommon.SSROrderedReadWriteMutex;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 */
public class SSRLocationService extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private static final Integer LOCATION_UPDATE_ALARM_INTERVAL_MS = 5000;
    private static final String START_SERVICE = "ssr.android.ssrcommonlibrary.services.action.START_SERVICE";
    private static final String FETCH_LOCATION = "ssr.android.ssrcommonlibrary.services.action.FETCH_LOCATION";
    private static final String STOP_SERVICE = "ssr.android.ssrcommonlibrary.services.action.STOP_SERVICE";
    public static final int REQUEST_CODE = R.integer.ssr_location_service_request_code;

    private GoogleApiClient m_googleApiClient;
    private SSROrderedReadWriteMutex m_locationMutex ;
    private GregorianCalendar m_lastLocationDateTime ;
    private Location m_lastLocation ;
    // Because the binder implementation class has no member variables or context in it and only
    // one method, we are creating it once and returning it. If this changes in future, this should
    // also change.
    private SSRLocationServiceBoundImplementationClass m_binderToReturn ;

    private class SSRLocationServiceBoundImplementationClass extends Binder
            implements ISSRLocationServiceBoundInterface
    {
        @Override
        public Map.Entry<GregorianCalendar, Location> getLastLocationDetails()
        {
            ensureGoogleApiConnected();
            AbstractMap.SimpleEntry<GregorianCalendar, Location> toReturn = null ;
            Boolean mutexAcquired = false ;
            try
            {
                try
                {
                    m_locationMutex.acquire(SSROrderedReadWriteMutex.ResourceOperation.READ, 500) ;
                    mutexAcquired = true ;
                    toReturn = new AbstractMap.SimpleEntry<GregorianCalendar, Location>(m_lastLocationDateTime,
                            m_lastLocation) ;
                }
                catch(Exception e)
                {
                    Log.e("ssrana", "Exception caught in inner loop 1 of " +
                            "SSRLocationServiceBoundIimplementationClass.getLastLocationDetails :\n", e) ;
                }
                finally
                {
                    if(true == mutexAcquired)
                        m_locationMutex.release() ;
                }
            }
            catch(Exception f)
            {
                Log.e("ssrana", "Exception caught in SSRLocationServiceBoundImplementationClass.getLastLocationDetails", f) ;
            }
            return toReturn ;
        }
    }

    public static Boolean startLocationService(Activity i_activityToUse, Hashtable<String, String> i_parameters)
            throws Exception
    {
        Boolean permissionGood = true ;

        if(ActivityCompat.checkSelfPermission(i_activityToUse, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED)
        {
            permissionGood = false ;
            ActivityCompat.requestPermissions(i_activityToUse, new String[]
                    {Manifest.permission.ACCESS_FINE_LOCATION}, 123) ;
        }

        if(false == permissionGood)
        {
            return false;
        }

        // Start service
        Intent intent = SSRLocationService.getIntentForStartingAsService(i_activityToUse, null,
                SSRLocationService.START_SERVICE) ;
        i_activityToUse.startService(intent) ;

        // Register service with AlarmManager, to get periodic location update

        // Create PendingIntent
        PendingIntent pendingIntentForAlarmManager = SSRLocationService.getPendingIntentForAlarmManager(
                i_activityToUse, null) ;

        // Pass PendingIntent to AlarmManager to activate period location retrieval
        AlarmManager alarmManager = (AlarmManager) (i_activityToUse.getSystemService(Context.ALARM_SERVICE));
        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SSRLocationService.LOCATION_UPDATE_ALARM_INTERVAL_MS * 3, SSRLocationService.LOCATION_UPDATE_ALARM_INTERVAL_MS,
                pendingIntentForAlarmManager);

        return true ;
    }

    public static void stopLocationService(Context i_context, Hashtable<String, String> i_parameters)
            throws Exception
    {
        // Stop alarmmanager from invoking the service

        Intent stopAlarmIntent = new Intent(i_context, SSRLocationService.class);
        stopAlarmIntent.setAction(SSRLocationService.START_SERVICE);
        AlarmManager alarmManager = (AlarmManager) (i_context.getSystemService(Context.ALARM_SERVICE));
        PendingIntent pendingIntent = PendingIntent.getService(i_context, SSRLocationService.REQUEST_CODE,
                stopAlarmIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        alarmManager.cancel(pendingIntent);

        // Stop the service itself
        Intent stopServiceIntent = new Intent(i_context, SSRLocationService.class);
        stopServiceIntent.setAction(SSRLocationService.STOP_SERVICE);
        SSRLocationService.setParametersInIntent(stopServiceIntent, i_parameters);
        i_context.startService(stopServiceIntent);
    }

    protected static Intent getIntentForStartingAsService(Context i_context, Hashtable<String,
            String> i_parameters, String i_actionToSet) throws Exception
    {
        Intent intent = new Intent(i_context, SSRLocationService.class);
        intent.setAction(i_actionToSet);
        SSRLocationService.setParametersInIntent(intent, i_parameters);
        return intent;
    }

    protected static PendingIntent getPendingIntentForAlarmManager(Context i_context, Hashtable<String,
            String> i_parameters) throws Exception
    {
        Intent intent = SSRLocationService.getIntentForStartingAsService(i_context, i_parameters,
                SSRLocationService.FETCH_LOCATION) ;
        PendingIntent pendingIntent = PendingIntent.getService(i_context, SSRLocationService.REQUEST_CODE,
                intent, PendingIntent.FLAG_CANCEL_CURRENT) ;
        return pendingIntent ;
    }

    public SSRLocationService()
    {
        super();
        this.m_locationMutex = new SSROrderedReadWriteMutex() ;
        this.m_lastLocation = null ;
        this.m_lastLocationDateTime = null ;
        this.m_binderToReturn = new SSRLocationServiceBoundImplementationClass() ;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (null == this.m_googleApiClient) {
            this.m_googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        super.onStartCommand(intent, flags, startId);

        String action = intent.getAction();
        //This method may be called multiple times during the service lifecycle so we must not
        //try to connect a m_googleApiClient if it is already connected or in the process of connecting
        if (SSRLocationService.START_SERVICE.equals(action))
        {
            Log.e("ssrana", "SSRLocationService received START_SERVICE intent") ;
            this.ensureGoogleApiConnected();
            return START_NOT_STICKY;
        }
        else if (SSRLocationService.FETCH_LOCATION.equals(action))
        {
            Log.e("ssrana", "SSRLocationService received FETCH_LOCATION intent") ;
            this.handleFetchLocation();
            return START_NOT_STICKY;
        }
        else if (SSRLocationService.STOP_SERVICE.equals(action))
        {
            Log.e("ssrana", "SSRLocationService received STOP_SERVICE intent") ;
            this.cleanup() ;
            this.stopSelf();
            return START_NOT_STICKY;
        }
        else
        {
            Log.e("ssrana", "Unknown action requested from SSRLocationService");
            return START_NOT_STICKY;
        }
    }

    @Override
    public IBinder onBind(Intent intent)
    {
        try
        {
            this.ensureGoogleApiConnected();
        }
        catch (Exception e)
        {
            Log.e("ssrana", "Exception caught in SSRLocationService.onBind");
        }
        finally
        {
            return this.m_binderToReturn ;
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle)
    {
        this.handleFetchLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e("ssrana", "Connection Suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult)
    {
        Log.e("ssrana", "Connection Failed");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cleanup();
    }

    @Override
    public void onLocationChanged(Location location)
    {
        Log.e("ssrana", "Location received. Latitude = " + location.getLatitude() + " Longitude = " + location.getLongitude());
        this.handleLocationChanged(location);
    }

    private void handleLocationChanged(Location i_location)
    {
        try
        {
            Boolean mutexAcquired = false;
            try
            {
                this.m_locationMutex.acquire(SSROrderedReadWriteMutex.ResourceOperation.WRITE, 100);
                mutexAcquired = true ;
                this.m_lastLocationDateTime = new GregorianCalendar() ;
                this.m_lastLocation = i_location ;
            }
            catch(Exception e)
            {
                Log.e("ssrana", "Exception caught in inner block 1 of SSRLocationService.handleLocatinChanged :\n", e) ;
            }
            finally
            {
                if (true == mutexAcquired)
                    this.m_locationMutex.release();
            }
        }
        catch(Exception f)
        {
            Log.e("ssrana", "Exception caught in SSRLocationService.handleLocationChanged :\n", f) ;
        }
    }

    private void ensureGoogleApiConnected() {
        if (!(true == this.m_googleApiClient.isConnected() || true == this.m_googleApiClient.isConnecting()))
        {
            this.m_googleApiClient.connect();
        }
    }

    private static void setParametersInIntent(Intent intent, Hashtable<String, String> parameters) {
        if (parameters == null) {
            return;
        }
        for (Enumeration<String> keys = parameters.keys(); keys.hasMoreElements(); ) {
            String key = keys.nextElement();
            intent.putExtra(key, parameters.get(key));
        }
    }


    /**
     * Handle action "location update" in the provided background thread.
     */
    private void handleFetchLocation()
    {
        // TODO : Put a check here to see that google location services is working. If not, the callback will never happen
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Log.e("ssrana", "Code does not have permission to access location") ;
            return;
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(this.m_googleApiClient,
                this.createLocationRequest(), this);
    }

    private LocationRequest createLocationRequest()
    {
        LocationRequest locationRequest = new LocationRequest();
        Long locationRequestInterval = 5000l ;

        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(locationRequestInterval) ;
        locationRequest.setNumUpdates(1);

        return locationRequest;
    }

    private void cleanup()
    {
        if (m_googleApiClient != null && m_googleApiClient.isConnected())
        {
            m_googleApiClient.disconnect();
        }
    }
}
