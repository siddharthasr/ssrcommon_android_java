package ssr.android.ssrcommonlibrary.handlers;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.Toast;

/**
 * Created on 7/30/2016.
 */
public class ShowToastHandler extends Handler
{
    public static final String SHOW_TOAST_HANDLER_NAME = "ShowToastHandler" ;

    public ShowToastHandler(Looper i_looperToUse)
    {
        super(i_looperToUse) ;
    }

    public void handleMessage(Message inputMessage)
    {
        Object[] parameterObjectsArray = (Object[])inputMessage.obj ;
        Context contextToUse = (Context)parameterObjectsArray[0] ;
        String messageToShow = (String) parameterObjectsArray[1] ;
        if (null == messageToShow)
            messageToShow = "No text sent to toast." ;

        Toast toast = Toast.makeText(contextToUse, messageToShow, Toast.LENGTH_SHORT) ;
        toast.show();
    }
}