package ssr.android.ssrcommonlibrary.applicationcustomization;

import android.app.Application;
import android.util.Log;

import ssr.android.ssrcommonlibrary.dataservices.SSRDataManager;

/**
 * Created by Siddhartha on 7/12/2017.
 * Purpose : This application class will provide basic customized functionality that we might need to
 *           put in application class
 */
public class SSRBasicApplication extends Application
{
    private SSRDataManager m_dataManager ;

    public SSRBasicApplication()
    {
        super() ;
        try
        {
            this.m_dataManager = SSRDataManager.getInstance();
        }
        catch(Exception e)
        {
            Log.e("ssrana", "Exception caught in SSRBasicApplication.SSRBasicApplication while trying to obtain a reference "
            + "of the DataManager class :\n", e) ;
        }
    }
}
