package ssr.android.ssrcommonlibrary.customviews;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashMap;

import ssr.android.ssrcommonlibrary.R;

/**
 * Created on 8/13/2016.
 */
public class SSRMixedListEditBoxRow extends SSRMixedListRow
{
    public Integer m_rowLayoutResourceId ;
    public Integer m_titleViewResourceId ;
    public Integer m_editBoxResourceId ;
    public TextView m_title ;
    public EditText m_editBox ;
    public String m_stringForEditBoxOnCreate ;

    public SSRMixedListEditBoxRow() throws Exception
    {
        super() ;
    }

    public SSRMixedListEditBoxRow(String i_titleString) throws Exception
    {
        super(i_titleString) ;
    }

    @Override
    public void putResult(HashMap<String, Object> o_hashMapToPutResultsIn) throws Exception
    {
        o_hashMapToPutResultsIn.put(this.toString(), this.m_editBox.getText().toString()) ;
    }

    @Override
    public View getViewToShow(LayoutInflater i_inflaterToUse, ViewGroup i_parentForView) throws Exception
    {
        View mainLayoutView = super.getViewToShow(i_inflaterToUse, i_parentForView) ;

        View viewToReturn = mainLayoutView.findViewById(null == this.m_rowLayoutResourceId
                ? R.id.mixedrows_editbox_layout : this.m_rowLayoutResourceId) ;
        if(null != this.m_editBox && null != this.m_title)
            return viewToReturn ;

        this.m_title = (TextView)(viewToReturn.findViewById(null == this.m_titleViewResourceId
                ? R.id.mixedrows_editbox_textbox : this.m_titleViewResourceId)) ;
        this.m_editBox = (EditText)(viewToReturn.findViewById(null == this.m_editBoxResourceId
                ? R.id.mixedrows_editbox_boxtoedit : this.m_titleViewResourceId)) ;

        this.m_title.setText(this.toString()) ;
        if(null != this.m_stringForEditBoxOnCreate)
            this.m_editBox.setText(this.m_stringForEditBoxOnCreate) ;

        if(null != this.m_onCreateHelper)
            this.m_onCreateHelper.onCreateHandling(this, i_inflaterToUse, i_parentForView) ;

        return viewToReturn ;
    }
}
