package ssr.android.ssrcommonlibrary.customviews;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import java.util.HashMap;

import ssr.android.ssrcommonlibrary.R;

/**
 * Created on 8/12/2016.
 */
public class SSRMixedListCheckboxRow extends SSRMixedListRow
{
    public Integer m_checkboxResourceId ;
    public CheckBox m_checkboxView ;
    public Boolean m_isCheckedOnCreation ;

    public SSRMixedListCheckboxRow() throws Exception
    {
        super() ;
    }

    public SSRMixedListCheckboxRow(String i_rowTextToStore, Integer i_checkboxResourceId,
                                   Boolean i_toCheckOnCreation) throws Exception
    {
        super(i_rowTextToStore) ;
        this.m_checkboxResourceId = i_checkboxResourceId ;
        this.m_isCheckedOnCreation = i_toCheckOnCreation ;
    }

    @Override
    public void putResult(HashMap<String, Object> o_hashMapToPutResultsIn) throws Exception
    {
        o_hashMapToPutResultsIn.put(this.toString(), new Boolean(((CheckBox)this.m_checkboxView).isChecked())) ;
    }

    @Override
    public View getViewToShow(LayoutInflater i_inflaterToUse, ViewGroup i_parentForView) throws Exception
    {
        View mainLayoutView = super.getViewToShow(i_inflaterToUse, i_parentForView) ;
        //viewToReturn.setOnClickListener(this) ;
        View viewToReturn = mainLayoutView.findViewById(R.id.mixedrows_checkbox_layout) ;
        CheckBox checkboxPart = ((CheckBox)viewToReturn.findViewById(R.id.mixedrows_checkbox_checkboxwidget)) ;
        checkboxPart.setText(this.m_listRowText) ;
        checkboxPart.setChecked(this.m_isCheckedOnCreation) ;

        this.m_rowView = viewToReturn ;
        this.m_checkboxView = checkboxPart ;

        return viewToReturn ;
    }
}
