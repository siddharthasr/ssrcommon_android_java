package ssr.android.ssrcommonlibrary.handlers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import ssr.android.ssrcommonlibrary.SSRFunctionalityLibrary;

/**
 * Created on 7/30/2016.
 */
public class WaitingDialogHandlerForUIThread extends Handler
{
    public static final String WAITING_DIALOG_HANDLE_NAME = "WaitingDialogHandler" ;
    public static final int WAITING_DIALOG_SHOW = 1 ;
    public static final int WAITING_DIALOG_DISMISS = 2 ;

    protected ProgressDialog m_progressDialogToShow ;
    protected Context m_contextUsedWhileCreatingWaitingDialog ;

    public WaitingDialogHandlerForUIThread()
    {
        super(Looper.getMainLooper()) ;
        this.m_contextUsedWhileCreatingWaitingDialog = null ;
    }

    public void handleMessage(Message inputMessage)
    {
        switch(inputMessage.what)
        {
            case WaitingDialogHandlerForUIThread.WAITING_DIALOG_SHOW :
            {
                Object[] parameterObjectsArray = (Object[])inputMessage.obj ;
                Context contextToUse = (Context)parameterObjectsArray[0] ;
                String messageToShow = (String) parameterObjectsArray[1] ;
                if (null == messageToShow)
                    messageToShow = "Please wait ..." ;
                this.m_progressDialogToShow = SSRFunctionalityLibrary.createAndReturnWaitingDialog(contextToUse);
                this.m_progressDialogToShow.setMessage(messageToShow);
                this.m_progressDialogToShow.show();
                this.m_contextUsedWhileCreatingWaitingDialog = contextToUse ;
                break;
            }
            case WaitingDialogHandlerForUIThread.WAITING_DIALOG_DISMISS :
            {
                if (true == this.m_progressDialogToShow.isShowing())
                {
                    this.m_progressDialogToShow.dismiss();
                    if(null != inputMessage.obj)
                    {
                        if(true == inputMessage.obj instanceof Intent
                                && null != this.m_contextUsedWhileCreatingWaitingDialog)
                        {
                            Intent intentForWaitingApplication = (Intent)inputMessage.obj ;
                            this.m_contextUsedWhileCreatingWaitingDialog.startActivity(intentForWaitingApplication) ;
                            this.m_contextUsedWhileCreatingWaitingDialog = null ;
                        }
                        if(true == inputMessage.obj instanceof ISSRHandlerCallback)
                        {
                            ISSRHandlerCallback callbackObject = (ISSRHandlerCallback)inputMessage.obj ;
                            callbackObject.doPostHandlerProcessing(null) ;
                        }
                        if(true == inputMessage.obj instanceof Object[])
                        {
                            Object[] inputParameterObjectArray = (Object[])inputMessage.obj ;
                            if(1 <= inputParameterObjectArray.length)
                            {
                                Object firstParameter = inputParameterObjectArray[0] ;

                                if(true == firstParameter instanceof ISSRHandlerCallback)
                                {
                                    ((ISSRHandlerCallback)firstParameter).doPostHandlerProcessing(
                                            inputParameterObjectArray) ;
                                }
                            }
                        }
                    }
                }
                break;
            }
            default :
                // Why are we here? This is a mistake
                int a = 20 ;
                a++ ;
                break ;
        }
    }
}
