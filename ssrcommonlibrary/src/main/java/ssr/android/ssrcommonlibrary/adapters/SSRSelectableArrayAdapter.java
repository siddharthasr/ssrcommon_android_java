package ssr.android.ssrcommonlibrary.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import java.util.HashMap;
import java.util.Vector;

/**
 * Created on 6/10/2016.
 * Purpose : This adapter will allow items to be selected and will be able to return the list of selected items
 *
 *  20160806 : A defect was found wherein "SSRListViewAdapter is showing multiple selections by colouring of the
 *             background when choosing some elements and then scrolling through it". This was fixed by putting
 *             code to color the background of unselected cells as white. Earlier we were only making the back-
 *             ground of the selected cells as blue. But it seems like there is some kind of recycling of the
 *             cells in listview which was causing the blue backgrounded defect.
 */
public class SSRSelectableArrayAdapter<T> extends ArrayAdapter<T> implements AdapterView.OnItemClickListener
{
    protected HashMap<Integer, T> m_selectedItemsHashMap ;

    public SSRSelectableArrayAdapter(Context i_context, int i_resource, int i_textViewResourceId, T[] i_objects)
    {
        super(i_context, i_resource, i_textViewResourceId, i_objects) ;
        this.init();
    }

    protected void init()
    {
        this.m_selectedItemsHashMap = new HashMap<Integer, T>() ;
    }

    public View getView(int i_position, View i_convertView, ViewGroup i_parent)
    {
        View viewObtained = super.getView(i_position, i_convertView, i_parent);
        if(this.m_selectedItemsHashMap.containsKey(i_position))
        {
            viewObtained.setBackgroundColor(Color.BLUE) ;
        }
        else
        {
            viewObtained.setBackgroundColor(Color.WHITE) ;
        }
        return viewObtained ;
    }

    public void onItemClick(AdapterView<?> i_adapterView, View i_clickedView, int i_position, long i_arg3)
    {
        if(this.m_selectedItemsHashMap.containsKey(new Integer(i_position)))
        {
            this.m_selectedItemsHashMap.remove(new Integer(i_position)) ;
            i_clickedView.setBackgroundColor(Color.WHITE) ;
        }
        else
        {
            this.m_selectedItemsHashMap.put(new Integer(i_position), (T)i_adapterView.getItemAtPosition(i_position)) ;
            i_clickedView.setBackgroundColor(Color.BLUE) ;
        }
    }

    public Vector<T> getSelectedItems()
    {
        return new Vector<T>(this.m_selectedItemsHashMap.values()) ;
    }
}
