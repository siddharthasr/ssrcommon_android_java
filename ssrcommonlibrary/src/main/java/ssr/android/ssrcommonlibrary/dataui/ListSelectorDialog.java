package ssr.android.ssrcommonlibrary.dataui;

import android.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import ssr.android.ssrcommonlibrary.R;
import ssr.android.ssrcommonlibrary.adapters.SSRSelectableArrayAdapter;
import ssr.android.ssrcommonlibrary.dataservices.SSRDataManager;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import ssrcommon.design.SSRKeyControlledCallbackGateKeeper;
import ssrcommon.exceptions.SSRBasicException;

/**
 * Created by Siddhartha on 20170607.
 * Purpose : This class is going to provide a DialogFragment from which, user can display two lists
 *           of Strings, from which the user will be able to select data that he wants to select.
 */
public class ListSelectorDialog extends DialogFragment implements View.OnClickListener
{
    public static final String AVAILABLE_LIST_ITEMS = "AVAILABLE_LIST_ITEMS" ;
    public static final String SELECTED_LIST_ITEMS = "SELECTED_LIST_ITEMS" ;
    public static final String AVAILABLE_LIST_RESULT = "AVAILABLE_LIST_RESULT" ;
    public static final String SELECTED_LIST_RESULT = "SELECTED_LIST_RESULT" ;
    public static final String CALLBACK_IDENTIFIER = "CALLBACK_IDENTIFIER" ;
    public static final String CALLBACK_KEY = "CALLBACK_KEY" ;

    protected HashSet<String> m_availableItemsHashSet;
    protected HashSet<String> m_selectedItemsHashSet;
    protected String m_callbackIdentifier ;
    protected String m_callbackKey ;
    protected String m_availableItemsResultKey ;
    protected String m_selectedItemsResultKey ;

    protected final Integer m_rootDialogLayoutId ;
    protected final Integer m_availableItemsListViewId ;
    protected final Integer m_selectedItemsListViewId ;
    protected final Integer m_moveAvailableToSelectedButtonId ;
    protected final Integer m_moveSelectedToAvailableButtonId ;
    protected final Integer m_okSelectionButtonId ;
    protected final Integer m_cancelSelectionButtonId ;

    protected View m_rootView ;
    protected ListView m_availableItemsListView ;
    protected ListView m_selectedItemsListView ;
    protected Button m_moveAvailableToSelectedButton ;
    protected Button m_moveSelectedToAvailableButton ;
    protected Button m_okSelectionButton ;
    protected Button m_cancelSelectionButton ;

    public ListSelectorDialog() throws Exception
    {
        super() ;

        this.m_rootDialogLayoutId = R.layout.list_selector_dialog_layout ;
        this.m_availableItemsListViewId = R.id.available_items_list ;
        this.m_selectedItemsListViewId = R.id.selected_items_list ;
        this.m_moveAvailableToSelectedButtonId = R.id.move_available_to_selected_button ;
        this.m_moveSelectedToAvailableButtonId = R.id.move_selected_to_available_button ;
        this.m_okSelectionButtonId = R.id.ok_selection_button ;
        this.m_cancelSelectionButtonId = R.id.cancel_selection_button ;
    }

    @Override
    public void onCreate (Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState) ;

        Bundle args = this.getArguments() ;
        this.m_availableItemsHashSet = new HashSet<String>() ;
        this.m_availableItemsHashSet.addAll((Set<String>)args.getSerializable(ListSelectorDialog.AVAILABLE_LIST_ITEMS)) ;
        this.m_selectedItemsHashSet = new HashSet<String>() ;
        this.m_selectedItemsHashSet.addAll((Set<String>)args.getSerializable(ListSelectorDialog.SELECTED_LIST_ITEMS)) ;

        this.m_callbackIdentifier = null ;
        this.m_callbackKey = null ;

        if(true == args.containsKey(ListSelectorDialog.CALLBACK_IDENTIFIER))
        {
            this.m_callbackIdentifier = args.getString(ListSelectorDialog.CALLBACK_IDENTIFIER) ;
            this.m_callbackKey = args.getString(ListSelectorDialog.CALLBACK_KEY) ;
        }
        else if (true == args.containsKey(ListSelectorDialog.SELECTED_LIST_RESULT))
        {
            this.m_selectedItemsResultKey = args.getString(ListSelectorDialog.SELECTED_LIST_RESULT) ;
            if(true == args.containsKey(ListSelectorDialog.AVAILABLE_LIST_RESULT))
            {
                this.m_availableItemsResultKey = args.getString(ListSelectorDialog.AVAILABLE_LIST_RESULT) ;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater i_inflater, ViewGroup i_container, Bundle i_savedInstanceState)
    {
        try
        {
            this.m_rootView = i_inflater.inflate(this.m_rootDialogLayoutId, i_container, false);

            this.m_availableItemsListView = (ListView) this.m_rootView.findViewById(this.m_availableItemsListViewId);
            this.m_selectedItemsListView = (ListView) this.m_rootView.findViewById(this.m_selectedItemsListViewId);
            this.m_moveAvailableToSelectedButton = (Button) this.m_rootView.findViewById(this.m_moveAvailableToSelectedButtonId);
            this.m_moveSelectedToAvailableButton = (Button) this.m_rootView.findViewById(this.m_moveSelectedToAvailableButtonId);
            this.m_okSelectionButton = (Button) this.m_rootView.findViewById(this.m_okSelectionButtonId);
            this.m_cancelSelectionButton = (Button) this.m_rootView.findViewById(this.m_cancelSelectionButtonId);

            this.m_moveAvailableToSelectedButton.setOnClickListener(this);
            this.m_moveSelectedToAvailableButton.setOnClickListener(this);
            this.m_okSelectionButton.setOnClickListener(this);
            this.m_cancelSelectionButton.setOnClickListener(this);

            this.populateLists();
        }
        catch(Exception e)
        {
            Log.e("ssrana", "Exception caught in ListSelectorDialog.onCreateView :\n", e) ;
        }

        return this.m_rootView ;
    }

    @Override
    public void onClick(View view)
    {
        try
        {
            int viewId = view.getId() ;
            if(viewId == this.m_moveAvailableToSelectedButtonId)
            {
                this.handleMoveAvailableToSelected() ;
            }
            else if(viewId == this.m_moveSelectedToAvailableButtonId)
            {
                this.handleMoveSelectedToAvailable() ;
            }
            else if(viewId == this.m_okSelectionButtonId)
            {
                this.handleOkSelection() ;
            }
            else if(viewId == this.m_cancelSelectionButtonId)
            {
                this.handleCancelSelection() ;
            }
            else
            {
                throw new SSRBasicException("Unknown view encountered in ListSelectorDialog.onClick") ;
            }
        }
        catch(Exception e)
        {
            Log.e("ssrana", "Exception caught in ListSelectorDialog.onClick :\n", e) ;
        }
    }

    protected void populateLists() throws Exception
    {
        SSRSelectableArrayAdapter<Object> dataForAvailableList =
                new SSRSelectableArrayAdapter<Object> (this.getActivity(), R.layout.misc_components_layout,
                        R.id.simple_largetext_textview, this.m_availableItemsHashSet.toArray()) ;
        this.m_availableItemsListView.setAdapter(dataForAvailableList) ;
        this.m_availableItemsListView.setOnItemClickListener(dataForAvailableList);

        SSRSelectableArrayAdapter<Object> dataForSelectedList =
                new SSRSelectableArrayAdapter<Object> (this.getActivity(), R.layout.misc_components_layout,
                        R.id.simple_largetext_textview, this.m_selectedItemsHashSet.toArray()) ;
        this.m_selectedItemsListView.setAdapter(dataForSelectedList) ;
        this.m_selectedItemsListView.setOnItemClickListener(dataForSelectedList) ;
    }

    protected void handleMoveAvailableToSelected() throws Exception
    {
        Vector<Object> selectedAvailableItems =
                ((SSRSelectableArrayAdapter<Object>)this.m_availableItemsListView.getAdapter()).getSelectedItems() ;
        Iterator itemsToMoveIterator = selectedAvailableItems.iterator() ;
        while(itemsToMoveIterator.hasNext())
        {
            String oneItemToMove = new String(itemsToMoveIterator.next().toString()) ;
            this.m_selectedItemsHashSet.add(oneItemToMove) ;
            this.m_availableItemsHashSet.remove(oneItemToMove) ;
        }
        this.populateLists() ;
    }

    protected void handleMoveSelectedToAvailable() throws Exception
    {
        Vector<Object> selectedChosenItems =
                ((SSRSelectableArrayAdapter<Object>)this.m_selectedItemsListView.getAdapter()).getSelectedItems() ;
        Iterator itemsToMoveIterator = selectedChosenItems.iterator() ;
        while(itemsToMoveIterator.hasNext())
        {
            String oneItemToMove = new String(itemsToMoveIterator.next().toString()) ;
            this.m_availableItemsHashSet.add(oneItemToMove) ;
            this.m_selectedItemsHashSet.remove(oneItemToMove) ;
        }
        this.populateLists() ;
    }

    protected void handleOkSelection() throws Exception
    {
        if(null != this.m_callbackIdentifier)
        {
            SSRKeyControlledCallbackGateKeeper.getInstance().performCallback(this.m_callbackIdentifier,
                    this.m_callbackKey, new Object[]{this.m_availableItemsHashSet, this.m_selectedItemsHashSet});
        }
        else if(null != this.m_selectedItemsResultKey)
        {
            SSRDataManager dataManager = SSRDataManager.getInstance() ;
            dataManager.storeUnSynchronousData(this.m_selectedItemsResultKey, this.m_selectedItemsHashSet) ;
            if(null != this.m_availableItemsResultKey)
            {
                dataManager.storeUnSynchronousData(this.m_availableItemsResultKey, this.m_availableItemsHashSet) ;
            }
        }

        this.dismiss() ;
    }

    protected void handleCancelSelection() throws Exception
    {
        this.dismiss() ;
    }
}
